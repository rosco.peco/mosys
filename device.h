#ifndef INCLUDE_DEVICE_H
#define INCLUDE_DEVICE_H

#include "errno.h"
#include "rwlock.h"
#include "types.h"
#include <stddef.h>
#include <stdint.h>

#define DEVICE_NUM 16

typedef struct device_operations {
    ssize_t (*read)(int minor, void *buf, size_t count, unsigned long offset, int *errno_ptr);
    ssize_t (*write)(int minor, const void *buf, size_t count, unsigned long offset, int *errno_ptr);
    int (*ioctl)(int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr);
} device_operations;

typedef struct device {
    device_operations *dops;
} device;

int device_register(int major, device_operations *dops);
void device_unregister(int major);

ssize_t device_read(int major, int minor, void *buf, size_t count, unsigned long offset, int *errno_ptr);
ssize_t device_write(int major, int minor, const void *buf, size_t count, unsigned long offset, int *errno_ptr);
int device_ioctl(int major, int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr);

#endif
