#ifndef INCLUDE_RWLOCK_H
#define INCLUDE_RWLOCK_H

#include "mutex.h"
#include <stdbool.h>
#include <stdint.h>

typedef struct rwlock {
    mutex r;
    mutex g;
    size_t b;
} rwlock;

#define RWLOCK_INIT { MUTEX_INIT, MUTEX_INIT, 0 }

void rwlock_init(rwlock *rwl);
void rwlock_read_begin(rwlock *rwl);
void rwlock_read_end(rwlock *rwl);
void rwlock_write_begin(rwlock *rwl);
void rwlock_write_end(rwlock *rwl);

#endif
