#include "circbuf.h"
#include "device.h"
#include "devreg.h"
#include "errno.h"
#include "mfp68901.h"
#include "module.h"
#include "panic.h"
#include "string.h"
#include "uapi/asm/ioctl.h"
#include "uapi/mosys/spi/spidev.h"
#include <gpio.h>

static int dev_spidev_init(void);
static void dev_spidev_deinit(void);

static inline void spidev_sclk_set(bool value);
static inline void spidev_mosi_set(bool value);
static inline bool spidev_miso_get(void);
static inline void spidev_cs1_set(bool value);
static inline void spidev_cs0_set(bool value);
static inline void spidev_csX_set(int minor, bool value);
static inline void spidev_delay_half_cycle(void);
static inline uint8_t spidev_send_recv_byte(uint8_t send_value);
static void spidev_send_recv(size_t send_count, const char send_buf[send_count], size_t recv_count, char recv_buf[recv_count]);
static int spidev_transfer(int minor, struct spi_ioc_transfer *transfer, int *errno_ptr);
static int spidev_message(int minor, size_t count, struct spi_ioc_transfer transfer[count / sizeof(struct spi_ioc_transfer)], int *errno_ptr);

static ssize_t dev_spidev_read(int minor, void *buf, size_t count, unsigned long offset, int *errno_ptr);
static ssize_t dev_spidev_write(int minor, const void *buf, size_t count, unsigned long offset, int *errno_ptr);
static int dev_spidev_ioctl(int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr);

static module dev_spidev = {
    .init = dev_spidev_init,
    .deinit = dev_spidev_deinit,
    .name = "dev_spidev",
};
MODULE_DEFINE(dev_spidev);

static const int dev_spidev_major = 3;

static bool cpol = 0;
static bool cpha = 0;
static unsigned char bits_per_word = 8;

#define SCLK_GPIO   GPIO2
#define MOSI_GPIO   GPIO3
#define MISO_GPIO   GPIO4
#define CS1_GPIO    GPIO5
#define CS0_GPIO    GPIO1

static const uint8_t sclk_gpio  =   SCLK_GPIO;
static const uint8_t mosi_gpio  =   MOSI_GPIO;
static const uint8_t miso_gpio  =   MISO_GPIO;
static const uint8_t cs1_gpio   =   CS1_GPIO;
static const uint8_t cs0_gpio   =   CS0_GPIO;

static device_operations dev_spidev_dops = {
    .read = dev_spidev_read,
    .write = dev_spidev_write,
    .ioctl = dev_spidev_ioctl,
};

static int dev_spidev_init(void) {
    mfp68901_gpip_config_masked(
        (cpol * sclk_gpio) | (1 * cs1_gpio) | (1 * cs0_gpio),
        0,
        (1 * sclk_gpio) | (1 * mosi_gpio) | (0 * miso_gpio) | (1 * cs1_gpio) | (1 * cs0_gpio),
        (1 * sclk_gpio) | (1 * mosi_gpio) | (1 * miso_gpio) | (1 * cs1_gpio) | (1 * cs0_gpio)
    );
    
    if (dev_spidev_major != device_register(dev_spidev_major, &dev_spidev_dops)) {
        panic("Failed to register spidev devices");
    }

    devreg_new("spidev0.0", dev_spidev_major, 0);
    devreg_new("spidev0.1", dev_spidev_major, 1);

    return 0;
}

static void dev_spidev_deinit(void) {
    device_unregister(dev_spidev_major);
}

static ssize_t dev_spidev_read(int minor, void *buf, size_t count, unsigned long offset, int *errno_ptr) {
    switch(minor) {
    case 0: // spidev0.0
    case 1: // spidev0.1
        break;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }
    spidev_csX_set(minor, false);
    spidev_send_recv(0, NULL, count, buf);
    spidev_csX_set(minor, true);
    return count;
}

static ssize_t dev_spidev_write(int minor, const void *buf, size_t count, unsigned long offset, int *errno_ptr) {
    switch(minor) {
    case 0: // spidev0.0
    case 1: // spidev0.1
        break;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }
    spidev_csX_set(minor, false);
    spidev_send_recv(count, buf, 0, NULL);
    spidev_csX_set(minor, true);
    return count;
}

static int dev_spidev_ioctl(int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr) {
    if (_IOC_TYPE(request) != SPI_IOC_MAGIC) {
        *errno_ptr = EINVAL;
        return -1;
    }

    switch(_IOC_NR(request)) {
    case 0:
        if (check_user_pointers && !user_ptr_is_valid(argp, _IOC_SIZE(request))) {
            *errno_ptr = EFAULT;
            return -1;
        }
        size_t size = _IOC_SIZE(request);
        size_t count = size / sizeof(struct spi_ioc_transfer);
        return spidev_message(minor, count, argp, errno_ptr);

    default:
        *errno_ptr = EINVAL;
        return -1;
    }
}

static inline void spidev_sclk_set(bool value) {
    mfp68901_gpdr_set_masked(0xFF * value, sclk_gpio);
}

static inline void spidev_mosi_set(bool value) {
    mfp68901_gpdr_set_masked(0xFF * value, mosi_gpio);
}

static inline bool spidev_miso_get(void) {
    bool value = mfp68901_gpdr_get() & miso_gpio;
    return value;
}

static inline void spidev_cs1_set(bool value) {
    mfp68901_gpdr_set_masked(0xFF * value, cs1_gpio);
}

static inline void spidev_cs0_set(bool value) {
    mfp68901_gpdr_set_masked(0xFF * value, cs0_gpio);
}

static inline void spidev_csX_set(int minor, bool value) {
    switch(minor) {
    case 0: // spidev0.0
        spidev_cs0_set(value);
        break;
    case 1: // spidev0.1
        spidev_cs1_set(value);
        break;
    }
}

static inline void spidev_delay_half_cycle(void) {
    // We're already slow enough
}

// Assumes correct CS is asserted, thread-safety has been ensured, and SCLK is in the idle state
static inline uint8_t spidev_send_recv_byte(uint8_t value) {
    const uint8_t msb_mask = 1U << (bits_per_word - 1); // Type must match value

    if (cpha == false) {
        for (unsigned char bit_index = 0; bit_index < bits_per_word; ++bit_index) {
            spidev_mosi_set(value & msb_mask);
            value <<= 1;
            spidev_delay_half_cycle();
            spidev_sclk_set(!cpol);
            value |= 0x01 * spidev_miso_get();
            spidev_delay_half_cycle();
            spidev_sclk_set(cpol);
        }
    } else {
        for (unsigned char bit_index = 0; bit_index < bits_per_word; ++bit_index) {
            spidev_sclk_set(!cpol);
            spidev_mosi_set(value & msb_mask);
            value <<= 1;
            spidev_delay_half_cycle();
            spidev_sclk_set(cpol);
            value |= 0x01 * spidev_miso_get();
            spidev_delay_half_cycle();
        }
    }

    return value;
}

// Assumes correct CS is asserted, thread-safety has been ensured, and SCLK is in the idle state
static void spidev_send_recv(size_t send_count, const char send_buf[send_count], size_t recv_count, char recv_buf[recv_count]) {
    for (size_t index = 0; index < send_count || index < recv_count; ++index) {
        uint8_t send_value = index < send_count ? send_buf[index] : 0;
        uint8_t recv_value = spidev_send_recv_byte(send_value);
        if (index < recv_count) {
            recv_buf[index] = recv_value;
        }
    }
}

static int spidev_transfer(int minor, struct spi_ioc_transfer *transfer, int *errno_ptr) {
    size_t tx_len = transfer->tx_buf ? transfer->len : 0;
    size_t rx_len = transfer->rx_buf ? transfer->len : 0;
    spidev_csX_set(minor, false);
    spidev_send_recv(tx_len, transfer->tx_buf, rx_len, transfer->rx_buf);
    if (transfer->cs_change) {
        spidev_csX_set(minor, true);
    }
    return 0;
}

static int spidev_message(int minor, size_t count, struct spi_ioc_transfer transfer[count], int *errno_ptr) {
    for (size_t transfers_done = 0; transfers_done < count; ++transfers_done) {
        int status = spidev_transfer(minor, &transfer[transfers_done], errno_ptr);
        if (status < 0) {
            return status;
        }
    }
    spidev_csX_set(minor, true);
    return 0;
}
