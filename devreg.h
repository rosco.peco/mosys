#ifndef INCLUDE_devreg_H
#define INCLUDE_devreg_H

#include "rwlock.h"

typedef struct devreg {
    rwlock rwl;
    size_t ref_count;

    const char *name;
    int major;
    int minor;

    struct devreg *next;
} devreg;

devreg *devreg_new(const char *name, int major, int minor);
void devreg_delete(devreg *dr);
void devreg_ref(devreg *dr);
void devreg_unref(devreg *dr);
devreg *devreg_lookup(const char *name);
devreg *devreg_get_nth(unsigned long n);

#endif
