#ifndef INCLUDE_EXCEPTION_H
#define INCLUDE_EXCEPTION_H

#include <stdint.h>

typedef void (*exception_func)();

struct exception_stack_frame {
    uint16_t sr;
    uint32_t pc;
    uint16_t format_vector_offset;
} __attribute__((packed));

void exception_init(void);
void exception_stack_frame_print(volatile struct exception_stack_frame *esf);

static inline exception_func exception_vector_replace(exception_func function, unsigned char vector) {
    exception_func * const exception_vector_table = (exception_func *) 0x00000000;
    exception_func old_function = exception_vector_table[vector];
    exception_vector_table[vector] = function;
    return old_function;
}

#endif
