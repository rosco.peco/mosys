#include "syscall.h"
#include "errno.h"
#include "exception.h"
#include "panic.h"
#include "process.h"
#include "processor.h"
#include "timing.h"
#include "types.h"
#include "vfs.h"
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define DEBUG_SYSCALLS 0
#define ALLOW_TRACE_ON_SYSCALLS 0
#define NO_CHECK_PTR 0

void syscall_init(void) {
    exception_vector_replace((exception_func) syscall_entry, 0x20);
}

long syscall_succeed(void);
long syscall_fail(long err);
long syscall_getpid(void);
long syscall_getppid(void);
// TODO: long syscall_open(const char *pathname, int flags, mode_t mode);
long syscall_open(const char *pathname, int flags, unsigned short mode);
long syscall_close(int fd);
long syscall_read(int fd, void *buf, size_t count);
long syscall_write(int fd, const void *buf, size_t count);
long syscall_nanosleep(const struct timespec *req, struct timespec *rem);
long syscall_clone(unsigned long flags, void *stack);
long syscall_execve(const char *pathname, char *const argv[],
                    char *const envp[]);
long syscall__exit(int status);
// TODO: Implement something like exit_group() if multi-threaded processes are added
long syscall_waitpid(pid_t pid, int *wstatus, int options);
long syscall_getdents(int fd, struct dirent *dirp, size_t count);
long syscall_ioctl(int fd, unsigned long request, void *argp);

long (*syscall_table[])() = {
    (long (*)()) syscall_succeed,
    (long (*)()) syscall_fail,
    (long (*)()) syscall_getpid,
    (long (*)()) syscall_getppid,
    (long (*)()) syscall_open,
    (long (*)()) syscall_close,
    (long (*)()) syscall_read,
    (long (*)()) syscall_write,
    (long (*)()) syscall_nanosleep,
    (long (*)()) syscall_clone,
    (long (*)()) syscall_execve,
    (long (*)()) syscall__exit,
    (long (*)()) syscall_waitpid,
    (long (*)()) syscall_getdents,
    (long (*)()) syscall_ioctl,
};

void syscall(volatile struct syscall_state state) {
    thread_current->user_state = &state;
    long number = state.d[0];

#if ALLOW_TRACE_ON_SYSCALLS
    if (number & 0x80000000) {
        // Enable trace bit until syscall is over
        asm ("ori #0x8000, %SR");
    }
#endif
    number &= ~0x80000000;

#if DEBUG_SYSCALLS
    printf("Syscall by PID %d: %ld(%ld, %ld, %ld, %ld, %ld, %ld)\n", thread_current->owner_process->pid, number, state.d[1], state.d[2], state.d[3], state.d[4], state.d[5], state.a[0]);
#endif

    if (number < 0 ||
        number >= sizeof(syscall_table) / sizeof(syscall_table[0]) ||
        !syscall_table[number]) {
        state.d[0] = -ENOSYS;
        return;
    }

    long result = syscall_table[number](state.d[1], state.d[2], state.d[3], state.d[4], state.d[5], state.a[0]);
    
#if DEBUG_SYSCALLS
    printf("Syscall by PID %d: %ld() -> %ld\n", thread_current->owner_process->pid, number, result);
#endif

    state.d[0] = result;
    thread_current->user_state = NULL;
}

long syscall_succeed() {
    return 0;
}

long syscall_fail(long err) {
    if (err <= 0) {
        return -EINVAL;
    }
    return -err;
}

long syscall_getpid(void) {
    return thread_current->owner_process->pid;
}

long syscall_getppid(void) {
    return thread_current->owner_process->parent->pid;
}

// TODO: long syscall_open(const char *pathname, int flags, mode_t mode) {
long syscall_open(const char *pathname, int flags, unsigned short mode) {
    size_t pathname_len;
    if (!user_strlen(pathname, &pathname_len)) {
        return -EFAULT;
    }

    int errno;
    int result = vfs_open(pathname_len, pathname, flags, mode, &errno);
    return result != -1 ? result : -errno;
}

long syscall_close(int fd) {
    int errno;
    ssize_t result = vfs_close(fd, &errno);
    return result != -1 ? result : -errno;
}

long syscall_read(int fd, void *buf, size_t count) {
    if (!user_ptr_is_valid(buf, count)) {
        return -EFAULT;
    }

    int errno;
    ssize_t result = vfs_read(fd, buf, count, &errno);
    return result != -1 ? result : -errno;
}

long syscall_write(int fd, const void *buf, size_t count) {
    if (!user_ptr_is_valid(buf, count)) {
        return -EFAULT;
    }

    int errno;
    ssize_t result = vfs_write(fd, buf, count, &errno);
    return result != -1 ? result : -errno;
}

long syscall_nanosleep(const struct timespec *req, struct timespec *rem) {
    // Validate req
    if (!user_ptr_is_valid(req, sizeof(*req))) {
        return -EFAULT;
    }
    // If rem is non-NULL, validate rem
    if (rem && !user_ptr_is_valid(rem, sizeof(*rem))) {
        return -EFAULT;
    }

    // Validate *req contents
    if (req->tv_nsec < 0 || req->tv_nsec > 999999999U || req->tv_sec < 0) {
        return -EINVAL;
    }

    // Divide nsec by 1000, rounding up
    unsigned long long usec = (req->tv_nsec + 999UL) / 1000U;
    // Multiply sec by 1000000
    usec += req->tv_sec * 1000000UL;    // * 1000000ULL

    timing_sleep_us(usec);
    return 0;
}

long syscall_clone(unsigned long flags, void *stack) {
    int errno;
    process *result = process_clone(flags, stack, &errno);
    return result ? result->pid : -errno;
}

long syscall_execve(const char *pathname, char *const argv[],
                    char *const envp[]) {
    size_t pathname_len;
    if (!user_strlen(pathname, &pathname_len)) {
        return -EFAULT;
    }

    size_t argv_len = 0;
    if (argv && !user_string_vector_len(argv, &argv_len)) {
        return -EFAULT;
    }

    size_t envp_len = 0;
    if (envp && !user_string_vector_len(envp, &envp_len)) {
        return -EFAULT;
}

    int errno;
    int result = process_execve(pathname_len, pathname, argv_len, argv, envp_len, envp, &errno);
    return result != -1 ? result : -errno;
}

long syscall__exit(int status) {
    process_exit_thread(status);
    panic("Returned from process_exit()");
}

long syscall_waitpid(pid_t pid, int *wstatus, int options) {
    if (wstatus && !user_ptr_is_valid(wstatus, sizeof(*wstatus))) {
        return -EFAULT;
    }

    int errno;
    pid_t result = process_waitpid(pid, wstatus, options, &errno);
    return result != -1 ? result : -errno;
}

long syscall_getdents(int fd, struct dirent *dirp, size_t count) {
    if (!user_ptr_is_valid(dirp, count)) {
        return -EFAULT;
    }

    int errno;
    ssize_t result = vfs_getdents(fd, dirp, count, &errno);
    return result != -1 ? result : -errno;
}

long syscall_ioctl(int fd, unsigned long request, void *argp) {
    int errno;
    int result = vfs_ioctl(fd, request, argp, true, &errno);
    return result != -1 ? result : -errno;
}

void syscall_print_state(volatile const struct syscall_state *state) {
    for (unsigned i = 0; i < 8; ++i) {
        printf("D%u  | %08lX\n", i, state->d[i]);
    }
    for (unsigned i = 0; i < 7; ++i) {
        printf("A%u  | %08lX\n", i, state->a[i]);
    }
    printf("USP | %08lX\n", state->usp);
    printf("SSP | %08lX\n", (unsigned long) state + sizeof(*state));
    printf("SR  | %04X\n", (unsigned short) state->sr);
    printf("PC  | %08X\n", state->pc);
    printf("FVO | %04X\n", (unsigned short) state->format_vector_offset);
}

bool object_is_in_range(const void *ptr, size_t size, ptr_range range) {
    const void *start = ptr;
    const void *end = (const char *) ptr + size;
    if (start >= range.start &&
        end < range.end) {
        return true;
    } else {
        return false;
    }
}

bool user_ptr_is_valid(const void *ptr, size_t size) {
#if NO_CHECK_PTR
    (void) object_is_in_range;
    return true;
#else
    if ((thread_current->usr_stack && object_is_in_range(ptr, size, thread_current->usr_stack->range)) || 
        (thread_current->owner_process->user_space && object_is_in_range(ptr, size, thread_current->owner_process->user_space->range)) || 
        (thread_current->owner_process->arg_env_space && object_is_in_range(ptr, size, thread_current->owner_process->arg_env_space->range))) {
        return true;
    } else {
        return false;
    }
#endif
}

bool user_strlen(const char *str, size_t *result) {
    // TODO: This is safe, but not "right", since it can waste time on bad input; probably need a custom strlen(), or to use strnlen() with a max size
    size_t tmp_result = strlen(str); 
    if (user_ptr_is_valid(str, tmp_result)) {
        if (result)
            *result = tmp_result;
        return true;
    } else {
        return false;
    }
}

bool user_string_vector_len(char *const *vect, size_t *result) {
    size_t tmp_result = 0;

    do {
        // Check that this pointer in array of pointers is valid in user space
        if (!user_ptr_is_valid(vect, sizeof(*vect)))
            return false;

        tmp_result += 1;

        // Exit if this pointer is NULL
        if (!*vect)
            break;

        // Check that this string is valid in user space
        if (!user_strlen(*vect, NULL))
            return false;

        vect += 1;
    } while (1);

    if (result)
        *result = tmp_result;
    return true;
}
