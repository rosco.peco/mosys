#include "module.h"
#include <stdio.h>

extern const module *_module_tab_start[1];
extern const module *_module_tab_end[1];

int module_init_static() {
    for (const module **module_tab_ptr = _module_tab_start; module_tab_ptr < _module_tab_end; ++module_tab_ptr) {
        const module *module_ptr = *module_tab_ptr;
        if (module_ptr->init) {
            printf("Module init: %p, %s\n", (void *) module_ptr, module_ptr->name);
            module_ptr->init();
        }
    }
    return 0;
}

void module_deinit_static() {
    for (const module **module_tab_ptr = _module_tab_end; module_tab_ptr > _module_tab_start; --module_tab_ptr) {
        const module *module_ptr = *(module_tab_ptr - 1);
        if (module_ptr->deinit) {
            printf("Module deinit: \"%s\"\n", module_ptr->name);
            module_ptr->deinit();
        }
    }
}
