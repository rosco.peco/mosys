#include "elf.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// TODO: This is a frankenstein combination of a few different tutorials/references for ELF parsing. Rewrite it.

#define ELF_RELOC_ERR (-1)

inline Elf32_Shdr *elf_sheader(Elf32_Ehdr *ehdr);
inline Elf32_Phdr *elf_pheader(Elf32_Ehdr *ehdr);
inline Elf32_Shdr *elf_section(Elf32_Ehdr *ehdr, size_t idx);
inline char *elf_str_table(Elf32_Ehdr *ehdr);
inline char *elf_lookup_string(Elf32_Ehdr *ehdr, size_t offset);
inline void *elf_lookup_symbol(const char *name);
int elf_get_symval(Elf32_Ehdr *ehdr, int table, size_t idx);
ownedmem *elf_load_stage1(Elf32_Ehdr *ehdr);
inline ownedmem *elf_load_rel(Elf32_Ehdr *ehdr);

inline Elf32_Shdr *elf_sheader(Elf32_Ehdr *ehdr) {
    return (Elf32_Shdr *) ((char *) ehdr + ehdr->e_shoff);
}

inline Elf32_Phdr *elf_pheader(Elf32_Ehdr *ehdr) {
    return (Elf32_Phdr *) ((char *) ehdr + ehdr->e_phoff);
}
 
inline Elf32_Shdr *elf_section(Elf32_Ehdr *ehdr, size_t idx) {
    return &elf_sheader(ehdr)[idx];
}

inline char *elf_str_table(Elf32_Ehdr *ehdr) {
    if (ehdr->e_shstrndx == SHN_UNDEF) {
        return NULL;
    }
    return (char *) ehdr + elf_section(ehdr, ehdr->e_shstrndx)->sh_offset;
}
 
inline char *elf_lookup_string(Elf32_Ehdr *ehdr, size_t offset) {
    char *strtab = elf_str_table(ehdr);
    if (strtab == NULL) {
        return NULL;
    }
    return strtab + offset;
}

inline void *elf_lookup_symbol(const char *name) {
    // We don't handle symbols yet
    return NULL;
}

int elf_get_symval(Elf32_Ehdr *ehdr, int table, size_t idx) {
    if (table == SHN_UNDEF || idx == SHN_UNDEF)
        return 0;
    Elf32_Shdr *symtab = elf_section(ehdr, table);
 
    uint32_t symtab_entries = symtab->sh_size / symtab->sh_entsize;
    if (idx >= symtab_entries) {
        // TODO: Direct this message somewhere more useful
        printf("Symbol index is out of range (%d:%u)\n", table, (unsigned) idx);
        return ELF_RELOC_ERR;
    }
 
    int symaddr = (int) ehdr + symtab->sh_offset;
    Elf32_Sym *symbol = &((Elf32_Sym *)symaddr)[idx];

    if(symbol->st_shndx == SHN_UNDEF) {
        // External symbol, lookup value
        Elf32_Shdr *strtab = elf_section(ehdr, symtab->sh_link);
        const char *name = (const char *)ehdr + strtab->sh_offset + symbol->st_name;
 
        void *target = elf_lookup_symbol(name);
 
        if(target == NULL) {
            // Extern symbol not found
            if(ELF32_ST_BIND(symbol->st_info) & STB_WEAK) {
                // Weak symbol initialized as 0
                return 0;
            } else {
                // TODO: Direct this message somewhere more useful
                printf("Undefined external symbol : %s\n", name);
                return ELF_RELOC_ERR;
            }
        } else {
            return (int)target;
        }
    } else if(symbol->st_shndx == SHN_ABS) {
        // Absolute symbol
        return symbol->st_value;
    } else {
        // Internally defined symbol
        Elf32_Shdr *target = elf_section(ehdr, symbol->st_shndx);
        return (int)ehdr + symbol->st_value + target->sh_offset;
    }
}

void elf_dynamic(Elf32_Ehdr *ehdr, char *virt_start, Elf32_Phdr *dyn_phdr, char *load) {
    Elf32_Dyn *dynamic = (Elf32_Dyn *) ((char *) ehdr + dyn_phdr->p_offset);

    Elf32_Rela *rela = NULL;
    size_t relasz = 0;
    size_t relaent = 0;

    for (size_t i = 0; i < dyn_phdr->p_filesz / sizeof(Elf32_Dyn); ++i) {
        switch (dynamic[i].d_tag) {
            case DT_NULL:
                break;
            case DT_RELA:
                rela = (Elf32_Rela *) (virt_start + dynamic[i].d_un.d_ptr);
                break;
            case DT_RELASZ:
                relasz = dynamic[i].d_un.d_val;
                break;
            case DT_RELAENT:
                relaent = dynamic[i].d_un.d_val;
                break;
        }
    }

    if (rela && relasz && relaent) {
        size_t relacount = relasz / relaent;
        for (size_t i = 0; i < relacount; ++i) {
            *(void **) (load + rela[i].r_offset) = (load + rela[i].r_addend);
        }
    }
}

ownedmem *elf_load_stage1(Elf32_Ehdr *ehdr) {
    Elf32_Phdr *phdr = elf_pheader(ehdr);
 
    ownedmem *load = NULL;
    char *virt_start = NULL;

    for (size_t i = 0; i < ehdr->e_phnum; ++i) {
        if (phdr[i].p_type == PT_LOAD) {
            load = ownedmem_new(phdr[i].p_memsz);
            // TODO: This leaks memory if multiple PT_LOAD exist

            memset(load->range.start, 0, phdr[i].p_memsz);
            memcpy((char *) load->range.start + phdr[i].p_vaddr, (char *) ehdr + phdr[i].p_offset, phdr[i].p_filesz);

            virt_start = (char *) ehdr + phdr[i].p_offset;
        } else if (phdr[i].p_type == PT_DYNAMIC) {
            if (!load->range.start || !virt_start) {
                printf("Reached DYNAMIC before LOAD\n");
                return NULL;
            }

            elf_dynamic(ehdr, virt_start, &phdr[i], load->range.start);
        }
    }

    return load;
}

inline ownedmem *elf_load_rel(Elf32_Ehdr *ehdr) {
    ownedmem *result = elf_load_stage1(ehdr);
    return result;
}
 
ownedmem *elf_load_file(void *file, void **entry) {
    Elf32_Ehdr *ehdr = (Elf32_Ehdr *)file;
    // if(!elf_check_supported(ehdr)) {
    //     ERROR("ELF File cannot be loaded.\n");
    //     return NULL;
    // }
    switch(ehdr->e_type) {
        case ET_EXEC:
            // TODO : Implement
            return NULL;
        case ET_REL:
        case ET_DYN: {
            ownedmem *result = elf_load_rel(ehdr);
            if (result) {
                *entry = (char *) result->range.start + ehdr->e_entry;
            }
            return result;
        }
    }
    return NULL;
}
