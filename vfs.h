#ifndef INCLUDE_VFS_H
#define INCLUDE_VFS_H

#include "devreg.h"
#include "mutex.h"
#include "types.h"
#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>

#define MAX_OPEN_FILES 16
#define NAME_MAX 32

struct superblock;
struct mountpoint;
struct vnode;
struct fdesc;
struct dirent;

enum {
    O_RDONLY    = 0x0001,
    O_WRONLY    = 0x0002,
    O_RDWR      = O_RDONLY | O_WRONLY,
};

typedef struct file_operations {
    int (*open)(struct fdesc *fdsc, int *errno_ptr);
    int (*close)(struct fdesc *fdsc, int *errno_ptr);
    ssize_t (*read)(struct fdesc *fdsc, void *buf, size_t count, int *errno_ptr);
    ssize_t (*write)(struct fdesc *fdsc, const void *buf, size_t count, int *errno_ptr);
    // Individual filesystems have to verify argp
    int (*ioctl)(struct fdesc *fdsc, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr);
    ssize_t (*getdents)(struct fdesc *fdsc, struct dirent *drnt, size_t count, int *errno_ptr);
    void (*release)(struct fdesc *fdsc);
} file_operations;

typedef struct fs_operations {
    struct superblock *(*mount)(const char *source, unsigned long mountflags, const void *data);
    // Should return with actual file info in fdesc, calling funciton will check and call destroy if needed
    struct vnode *(*lookup)(struct superblock *sb, const char *subpath);
    // Destroy should unref the passed fdesc
    void (*destroy)(struct superblock *sb, struct vnode *vn);
} fs_operations;

typedef struct filesystem_type {
    const char *name;
    fs_operations *fsops;

    struct filesystem_type *next;
} filesystem_type;

typedef struct superblock {
    mutex mtx;

    filesystem_type *type;
    struct vnode *mounted;
    struct vnode *covered;
    void *data;
} superblock;

typedef struct mountpoint {
    size_t ref_count;

    superblock *sb;
    const char *mount_path;

    struct mountpoint *next;
} mountpoint;

typedef enum file_type {
    FILE_TYPE_PLAIN,
    FILE_TYPE_DIRECTORY,
    FILE_TYPE_DEVICE,
} file_type;

typedef struct vnode {
    mutex mtx;
    size_t ref_count;

    file_type type;
    union {
        // struct {
        // } plain;
        // struct {
        // } directory;
        struct {
            devreg *dr;
        } device;
    };

    ino_t inode;
    superblock *sb;

    file_operations *fops;

    struct vnode *next; // To be used internally by filesystems
} vnode;

typedef struct fdesc {
    mutex mtx;
    size_t ref_count;

    // Fields set by filesystem on fsops->lookup()
    vnode *vn;

    // Fields set by filesystem on fsops->lookup() then refined by open() before fops->open()
    // TODO: The line above this one is wrong
    unsigned int mode;
    unsigned int flags;

    // Fields set by filesystem on fops->open()
    void *data_ptr;

    // Fields set by open() after fops->open()
    unsigned long offset;
} fdesc;

typedef struct fdesc_list {
    mutex mtx;
    size_t ref_count;

    fdesc *fds[MAX_OPEN_FILES];
} fdesc_list;

typedef struct dirent {
    ino_t ino;
    char name[NAME_MAX + 1];
} dirent;

void vfs_filesystem_type_register(filesystem_type *fst);

superblock *vfs_superblock_new(filesystem_type *type);
void vfs_superblock_delete(superblock *sb);

mountpoint *vfs_mountpoint_new(superblock *sb, const char *mount_path);
void vfs_mountpoint_delete(mountpoint *mp);
void vfs_mountpoint_ref_nolock(mountpoint *mp);     // mountpoint_list_rwl must be locked
void vfs_mountpoint_unref_nolock(mountpoint *mp);   // mountpoint_list_rwl must be locked
void vfs_mountpoint_ref(mountpoint *mp);
void vfs_mountpoint_unref(mountpoint *mp);
mountpoint *vfs_mountpoint_lookup(const char *path, const char **subpath_ptr);

vnode *vfs_vnode_new(superblock *sb);
void vfs_vnode_ref(vnode *vn);
void vfs_vnode_unref(vnode *vn);
void vfs_vnode_delete(vnode *vn);

fdesc *vfs_fdesc_new(void);
void vfs_fdesc_delete(fdesc *fdsc);
void vfs_fdesc_ref(fdesc *fdsc);
void vfs_fdesc_unref(fdesc *fdsc);
fdesc *vfs_fdesc_clone(fdesc *src_fdsc);

fdesc_list *vfs_fdesc_list_new(void);
void vfs_fdesc_list_delete(fdesc_list *fdl);
void vfs_fdesc_list_ref(fdesc_list *fdl);
void vfs_fdesc_list_unref(fdesc_list *fdl);
fdesc_list *vfs_fdesc_list_clone(fdesc_list *src_fdl);

int vfs_open(size_t pathname_len, const char [pathname_len], int flags, mode_t mode, int *errno_ptr);
int vfs_close(int fd, int *errno_ptr);
ssize_t vfs_read(int fd, void *buf, size_t count, int *errno_ptr);
ssize_t vfs_write(int fd, const void *buf, size_t count, int *errno_ptr);
int vfs_ioctl(int fd, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr);
ssize_t vfs_getdents(int fd, dirent *drnt, size_t count, int *errno_ptr);

fdesc *vfs_open_fdesc(size_t pathname_len, const char pathname[pathname_len], int flags, mode_t mode, int *errno_ptr);
int vfs_close_fdesc(fdesc *fdsc, int *errno_ptr);
ssize_t vfs_read_fdesc(fdesc *fdsc, void *buf, size_t count, int *errno_ptr);
ssize_t vfs_write_fdesc(fdesc *fdsc, const void *buf, size_t count, int *errno_ptr);
int vfs_ioctl_fdesc(fdesc *fdsc, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr);
ssize_t vfs_getdents_fdesc(fdesc *fdsc, dirent *drnt, size_t count, int *errno_ptr);

#endif
