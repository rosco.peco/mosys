#include <stdio.h>
#include <stdint.h>
#include <stdalign.h>
#include <stdbool.h>
#include <string.h>
#include <machine.h>
#include "device.h"
#include "elf.h"
#include "errno.h"
#include "exception.h"
#include "kalloc.h"
#include "mfp68901.h"
#include "module.h"
#include "mutex.h"
#include "panic.h"
#include "process.h"
#include "processor.h"
#include "syscall.h"
#include "thread.h"
#include "timing.h"

int errno = 0;

void init() {
    syscall_init();
    module_init_static();

    const char *init_path = "/init.elf";
    int result = process_execve(strlen(init_path), init_path, 0,  NULL, 0, NULL, &errno);

    printf("Failed to execute %s as PID 1: return=%d, errno=%d\n", init_path, result, errno);
    panic("Supervisor continued in init process");
}

void kmain() {
    printf("Starting Mosys\n");

    exception_init();

    thread_init();

    process *init_process = process_new();
    if (!init_process) {
        panic("Failed to create init process");
    }
    init_process->fdl = vfs_fdesc_list_new();
    if (!init_process->fdl) {
        panic("Failed to create fdesc_list for init process");
    }
    process_setup(init_process, init, ownedmem_new(1000), NULL);
    thread_unblock(init_process->owned_thread);

    thread_idle_loop();
}
