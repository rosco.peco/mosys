#ifndef INCLUDE_PROCESSOR_H
#define INCLUDE_PROCESSOR_H

#include <stdint.h>

__attribute__((always_inline)) inline void *usp_get(void) {
    void *usp;
    asm("move %%USP,%0"
        : "=a" (usp));
    return usp;
}

__attribute__((always_inline)) inline void usp_set(void *usp) {
    asm("move %0,%%USP"
        :
        : "a" (usp));
}

#endif
