    export  syscall_entry
    export  syscall_leave
    import  syscall
    
    section .text

syscall_entry:
    sub.l   #4,SP   ; Leave space for USP
    movem.l D0-D7/A0-A6,-(SP)
    move.l  USP,A1
    move.l  A1,(60,SP)

    jsr     syscall

syscall_leave:
    move.l  (60,SP),A1
    move.l  A1,USP
    movem.l (SP)+,D0-D7/A0-A6
    add.l   #4,SP   ; Remove space for USP

    rte
