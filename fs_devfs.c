#include "device.h"
#include "devreg.h"
#include "kalloc.h"
#include "module.h"
#include "panic.h"
#include "vfs.h"
#include <stdatomic.h>
#include <stdbool.h>
#include <string.h>

static int devfs_init(void);
static void devfs_deinit(void);

static vnode *devfs_fsops_lookup(superblock *sb, const char *subpath);
static void devfs_fsops_destroy(superblock *sb, vnode *vn);

static ssize_t devfs_fops_read(fdesc *fdsc, void *buf, size_t count, int *errno_ptr);
static ssize_t devfs_fops_write(fdesc *fdsc, const void *buf, size_t count, int *errno_ptr);
static int devfs_fops_ioctl(struct fdesc *fdsc, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr);
static ssize_t devfs_fops_getdents(fdesc *fdsc, dirent *drnt, size_t count, int *errno_ptr);

static atomic_bool devfs_is_init = false;

static module devfs = {
    .init = devfs_init,
    .deinit = devfs_deinit,
    .name = "devfs",
};
MODULE_DEFINE(devfs);

static fs_operations devfs_fsops = {
    .lookup = devfs_fsops_lookup,
    .destroy = devfs_fsops_destroy,
};

static file_operations devfs_fops = {
    .read = devfs_fops_read,
    .write = devfs_fops_write,
    .ioctl = devfs_fops_ioctl,
    .getdents = devfs_fops_getdents,
};

static filesystem_type devfs_fstype = {
    .name = "devfs",
    .fsops = &devfs_fsops,
};

static superblock *devfs_sb;
static mountpoint *devfs_mp;
static vnode *devfs_vn_root;

static int devfs_init(void) {
    vfs_filesystem_type_register(&devfs_fstype);

    // Only one devfs ever exists, so mount it here

    // Create dummy superblock
    devfs_sb = vfs_superblock_new(&devfs_fstype);
    if (!devfs_sb)
        goto failed_superblock;

    // Create root vnode
    devfs_vn_root = vfs_vnode_new(devfs_sb);
    if (!devfs_vn_root)
        goto failed_vnode;

    devfs_vn_root->type = FILE_TYPE_DIRECTORY;

    devfs_vn_root->inode = 0;

    devfs_vn_root->fops = &devfs_fops;

    // Link root vnode in superblock 
    devfs_sb->mounted = devfs_vn_root;

    // Create mountpoint
    devfs_mp = vfs_mountpoint_new(devfs_sb, "/dev/");
    if (!devfs_mp)
        goto failed_mountpoint;

    devfs_is_init = true;
    return 0;

failed_mountpoint:
    devfs_sb->mounted = NULL;
    vfs_vnode_delete(devfs_vn_root);
failed_vnode:
    vfs_superblock_delete(devfs_sb);
failed_superblock:
    return -1;
}

static void devfs_deinit(void) {
    devfs_is_init = false;

    // TODO: Figure out what to actually do here
    (void) devfs_fops;
}

static vnode *devfs_fsops_lookup(superblock *sb, const char *subpath) {
    if (0 == strcmp("", subpath)) {
        vfs_vnode_ref(sb->mounted);
        return sb->mounted;
    } else {
        devreg *dr = devreg_lookup(subpath);
        if (!dr) {
            return NULL;
        }

        vnode *vn = vfs_vnode_new(sb);
        if (!vn) {
            devreg_unref(dr);
            return NULL;
        }

        mutex_lock(&vn->mtx);
        vn->type = FILE_TYPE_DEVICE;
        vn->device.dr = dr;

        vn->inode = dr->major * 256 + dr->minor;

        vn->fops = &devfs_fops;
        mutex_unlock(&vn->mtx);
        return vn;
    }
}

static void devfs_fsops_destroy(superblock *sb, vnode *vn) {
    if (vn->type == FILE_TYPE_DIRECTORY) {
        vfs_vnode_unref(vn);
        vn = NULL;
    } else if (vn->type == FILE_TYPE_DEVICE) {
        devreg_unref(vn->device.dr);
        vn->device.dr = NULL;
    }
}

static ssize_t devfs_fops_read(fdesc *fdsc, void *buf, size_t count, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DEVICE)
        panic("Passed non-device fdesc as device");

    rwlock_read_begin(&fdsc->vn->device.dr->rwl);
    ssize_t result = device_read(fdsc->vn->device.dr->major, fdsc->vn->device.dr->minor, buf, count, fdsc->offset, errno_ptr);
    rwlock_read_end(&fdsc->vn->device.dr->rwl);
    return result;
}

static ssize_t devfs_fops_write(fdesc *fdsc, const void *buf, size_t count, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DEVICE)
        panic("Passed non-device fdesc as device");

    rwlock_read_begin(&fdsc->vn->device.dr->rwl);
    ssize_t result = device_write(fdsc->vn->device.dr->major, fdsc->vn->device.dr->minor, buf, count, fdsc->offset, errno_ptr);
    rwlock_read_end(&fdsc->vn->device.dr->rwl);
    return result;
}

static int devfs_fops_ioctl(struct fdesc *fdsc, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DEVICE)
        panic("Passed non-device fdesc as device");

    rwlock_read_begin(&fdsc->vn->device.dr->rwl);
    ssize_t result = device_ioctl(fdsc->vn->device.dr->major, fdsc->vn->device.dr->minor, request, argp, check_user_pointers, errno_ptr);
    rwlock_read_end(&fdsc->vn->device.dr->rwl);
    return result;
}

static ssize_t devfs_fops_getdents(fdesc *fdsc, dirent *drnt, size_t count, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DIRECTORY)
        panic("Expected directory");
    if (fdsc->vn != devfs_vn_root)
        panic("Expected devfs_vn_root as only directory");

    size_t done_count = 0;
    while (1) {
        devreg *dr = devreg_get_nth(fdsc->offset);
        if (!dr)
            break;
        size_t needed_count = sizeof(dirent);
        if (needed_count <= count) {
            count -= needed_count;
            drnt->ino = dr->major * 256 + dr->minor;
            strncpy(drnt->name, dr->name, sizeof(drnt->name));
            drnt->name[sizeof(drnt->name) - 1] = '\0';
            devreg_unref(dr);
            drnt = (dirent *) ((char *) drnt + needed_count);
            done_count += needed_count;
        } else {
            devreg_unref(dr);
            if (done_count == 0) {
                *errno_ptr = EINVAL;
                done_count = -1;
            }
            break;
        }
        fdsc->offset += 1;
    }

    return done_count;
}
