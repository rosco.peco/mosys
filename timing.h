#ifndef INCLUDE_TIMING_H
#define INCLUDE_TIMING_H

// Only to be called in scheduler context
void timing_uptime_tick_us(unsigned long long addend);
void timing_uptime_wake_sleeping(void);
unsigned long long timing_uptime_get_us(void);
void timing_sleep_until_us(unsigned long long time_us);

static inline void timing_sleep_us(unsigned long long time_offset_us) {
    timing_sleep_until_us(timing_uptime_get_us() + time_offset_us);
}

#endif
