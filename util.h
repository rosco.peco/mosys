#ifndef INCLUDE_UTIL_H
#define INCLUDE_UTIL_H

#define min(a, b)               \
    __extension__ ({            \
        __auto_type _a = (a);   \
        __auto_type _b = (b);   \
        _a < _b ? _a : _b;      \
    })

#define max(a, b)               \
    __extension__ ({            \
        __auto_type _a = (a);   \
        __auto_type _b = (b);   \
        _a > _b ? _a : _b;      \
    })

#endif
