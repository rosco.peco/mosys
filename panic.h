#ifndef INCLUDE_PANIC_H
#define INCLUDE_PANIC_H

#include <stdnoreturn.h>

noreturn void panic_func(const char *file, const char *line, const char *reason);
#define panic_XX(file, line, reason) panic_func(file, #line, reason)
#define panic_X(file, line, reason) panic_XX(file, line, reason)
#define panic(reason) panic_X(__FILE__, __LINE__, reason)

#endif
