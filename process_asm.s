    export  process_enter_state
    import syscall_leave
    
    section .text

; noreturn void process_enter_state(struct syscall_state *ssp)
process_enter_state:
    move.l  (4,SP),SP       ; ssp to SSP

    jmp     syscall_leave
