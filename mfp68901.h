#ifndef INCLUDE_MFP68901_h
#define INCLUDE_MFP68901_h

#include "exception.h"
#include <machine.h>
#include <stdbool.h>

#define MFP68901_USE_MOVEP 0

#define MFP68901_R(reg) (*((volatile uint8_t *) reg))

static inline unsigned long mfp68901_xtal_freq_get(void) {
    return 3686400;
}

////////////////////////////////////////////////////////////////////////////////
// INTERRUPT STRUCTURE                                                        //
////////////////////////////////////////////////////////////////////////////////

enum mfp68901_vr {
    MFP68901_VR_V_MASK          = 0xF0,

    MFP68901_VR_S               = 0x08, // In-Service Register Enable

    MFP68901_VR_MASK            = 0xF8,
    MFP68901_VR_MAX             = 0xF8,
};

enum mfp68901_interrupt {
    MFP68901_INTERRUPT_GPIP7            = 1 << 15,
    MFP68901_INTERRUPT_GPIP6            = 1 << 14,
    MFP68901_INTERRUPT_TIMER_A          = 1 << 13,
    MFP68901_INTERRUPT_RCV_BUF_FULL     = 1 << 12,
    MFP68901_INTERRUPT_RCV_ERR          = 1 << 11,
    MFP68901_INTERRUPT_XMIT_BUF_EMPTY   = 1 << 10,
    MFP68901_INTERRUPT_XMIT_ERR         = 1 << 9,
    MFP68901_INTERRUPT_TIMER_B          = 1 << 8,
    MFP68901_INTERRUPT_GPIP5            = 1 << 7,
    MFP68901_INTERRUPT_GPIP4            = 1 << 6,
    MFP68901_INTERRUPT_TIMER_C          = 1 << 5,
    MFP68901_INTERRUPT_TIMER_D          = 1 << 4,
    MFP68901_INTERRUPT_GPIP3            = 1 << 3,
    MFP68901_INTERRUPT_GPIP2            = 1 << 2,
    MFP68901_INTERRUPT_GPIP1            = 1 << 1,
    MFP68901_INTERRUPT_GPIP0            = 1 << 0,

    MFP68901_INTERRUPT_MASK             = 0xFFFF,
    MFP68901_INTERRUPT_MAX              = 0xFFFF,
};

static inline uint8_t mfp68901_vr_interrupt_vector_number_get(void) {
    return MFP68901_R(MFP_VR) & MFP68901_VR_V_MASK;
}

static inline void mfp68901_vr_interrupt_vector_number_set(uint8_t int_vect_num_upper_4) {
    MFP68901_R(MFP_VR) = (int_vect_num_upper_4 & MFP68901_VR_V_MASK) | (MFP68901_R(MFP_VR) & ~MFP68901_VR_V_MASK);
}

static inline bool mfp68901_vr_in_service_enable_get(void) {
    return MFP68901_R(MFP_VR) & MFP68901_VR_S;
}

static inline void mfp68901_vr_in_service_enable_set(bool enable) {
    MFP68901_R(MFP_VR) = (enable ? MFP68901_VR_S : 0) | (MFP68901_R(MFP_VR) & ~MFP68901_VR_S);
}

static inline enum mfp68901_interrupt mfp68901_ier_get(void) {
    enum mfp68901_interrupt val = 0;
#if MFP68901_USE_MOVEP
    asm volatile (
        "movepw (0,%[reg]), %[val] \n"
        : [val] "+d" (val)
        : [reg] "a" (MFP_IERA)
    );
#else
    val = MFP68901_R(MFP_IERA) << 8 | MFP68901_R(MFP_IERB);
#endif
    return val;
}

static inline enum mfp68901_interrupt mfp68901_ier_set(enum mfp68901_interrupt val) {
#if MFP68901_USE_MOVEP
    asm volatile (
        "movepw %[val], (0,%[reg]) \n"
        :
        : [reg] "a" (MFP_IERA), [val] "d" (val)
        : "memory"
    );
#else
    MFP68901_R(MFP_IERA) = val >> 8;
    MFP68901_R(MFP_IERB) = val;
#endif
    return val;
}

static inline enum mfp68901_interrupt mfp68901_ier_bitset(enum mfp68901_interrupt mask) {
    enum mfp68901_interrupt val = mfp68901_ier_get();
    val |= mask;
    mfp68901_ier_set(val);
    return val;
}

static inline enum mfp68901_interrupt mfp68901_ier_bitclear(enum mfp68901_interrupt mask) {
    enum mfp68901_interrupt val = mfp68901_ier_get();
    val &= ~mask;
    mfp68901_ier_set(val);
    return val;
}

static inline enum mfp68901_interrupt mfp68901_ipr_get(void) {
    enum mfp68901_interrupt val = 0;
#if MFP68901_USE_MOVEP
    asm volatile (
        "movepw (0,%[reg]), %[val] \n"
        : [val] "+d" (val)
        : [reg] "a" (MFP_IPRA)
    );
#else
    val = MFP68901_R(MFP_IPRA) << 8 | MFP68901_R(MFP_IPRB);
#endif
    return val;
}

static inline enum mfp68901_interrupt mfp68901_ipr_set(enum mfp68901_interrupt val) {
#if MFP68901_USE_MOVEP
    asm volatile (
        "movepw %[val], (0,%[reg]) \n"
        :
        : [reg] "a" (MFP_IPRA), [val] "d" (val)
        : "memory"
    );
#else
    MFP68901_R(MFP_IPRA) = val >> 8;
    MFP68901_R(MFP_IPRB) = val;
#endif
    return val;
}

static inline enum mfp68901_interrupt mfp68901_ipr_bitclear(enum mfp68901_interrupt mask) {
    enum mfp68901_interrupt val = mfp68901_ipr_get();
    val &= ~mask;
    mfp68901_ipr_set(val);
    return val;
}

static inline enum mfp68901_interrupt mfp68901_isr_get(void) {
    enum mfp68901_interrupt val = 0;
#if MFP68901_USE_MOVEP
    asm volatile (
        "movepw (0,%[reg]), %[val] \n"
        : [val] "+d" (val)
        : [reg] "a" (MFP_IPRA)
    );
#else
    val = MFP68901_R(MFP_ISRA) << 8 | MFP68901_R(MFP_ISRB);
#endif
    return val;
}

static inline enum mfp68901_interrupt mfp68901_isr_set(enum mfp68901_interrupt val) {
#if MFP68901_USE_MOVEP
    asm volatile (
        "movepw %[val], (0,%[reg]) \n"
        :
        : [reg] "a" (MFP_ISRA), [val] "d" (val)
        : "memory"
    );
#else
    MFP68901_R(MFP_ISRA) = val >> 8;
    MFP68901_R(MFP_ISRB) = val;
#endif
    return val;
}

static inline enum mfp68901_interrupt mfp68901_isr_bitclear(enum mfp68901_interrupt mask) {
    enum mfp68901_interrupt val = mfp68901_isr_get();
    val &= ~mask;
    mfp68901_isr_set(val);
    return val;
}

static inline enum mfp68901_interrupt mfp68901_imr_get(void) {
    enum mfp68901_interrupt val = 0;
#if MFP68901_USE_MOVEP
    asm volatile (
        "movepw (0,%[reg]), %[val] \n"
        : [val] "+d" (val)
        : [reg] "a" (MFP_IPRA)
    );
#else
    val = MFP68901_R(MFP_IMRA) << 8 | MFP68901_R(MFP_IMRB);
#endif
    return val;
}

static inline enum mfp68901_interrupt mfp68901_imr_set(enum mfp68901_interrupt val) {
#if MFP68901_USE_MOVEP
    asm volatile (
        "movepw %[val], (0,%[reg]) \n"
        :
        : [reg] "a" (MFP_IMRA), [val] "d" (val)
        : "memory"
    );
#else
    MFP68901_R(MFP_IMRA) = val >> 8;
    MFP68901_R(MFP_IMRB) = val;
#endif
    return val;
}

static inline enum mfp68901_interrupt mfp68901_imr_bitset(enum mfp68901_interrupt mask) {
    enum mfp68901_interrupt val = mfp68901_imr_get();
    val |= mask;
    mfp68901_imr_set(val);
    return val;
}

static inline enum mfp68901_interrupt mfp68901_imr_bitclear(enum mfp68901_interrupt mask) {
    enum mfp68901_interrupt val = mfp68901_imr_get();
    val &= ~mask;
    mfp68901_imr_set(val);
    return val;
}

static inline void mfp68901_interrupt_install(exception_func handler, enum mfp68901_interrupt vectors) {
    uint8_t vector_base = mfp68901_vr_interrupt_vector_number_get();
    for (unsigned i = 0; i < 16 && vectors; ++i) {
        if (vectors & (1U << i)) {
            exception_vector_replace(handler, vector_base + i);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// GENERAL PURPOSE INPUT/OUTPUT INTERRUPT PORT                                //
////////////////////////////////////////////////////////////////////////////////

static inline uint8_t mfp68901_gpdr_get(void) {
    return MFP68901_R(MFP_GPDR);
}

static inline void mfp68901_gpdr_set(uint8_t gpdr) {
    MFP68901_R(MFP_GPDR) = gpdr;
}

static inline void mfp68901_gpdr_set_masked(uint8_t gpdr, uint8_t mask) {
    MFP68901_R(MFP_GPDR) = (MFP68901_R(MFP_GPDR) & ~mask) | (gpdr & mask);
}

static inline uint8_t mfp68901_aer_get(void) {
    return MFP68901_R(MFP_AER);
}

static inline void mfp68901_aer_set(uint8_t aer) {
    MFP68901_R(MFP_AER) = aer;
}

static inline void mfp68901_aer_set_masked(uint8_t aer, uint8_t mask) {
    MFP68901_R(MFP_AER) = (MFP68901_R(MFP_AER) & ~mask) | (aer & mask);
}

static inline uint8_t mfp68901_ddr_get(void) {
    return MFP68901_R(MFP_DDR);
}

static inline void mfp68901_ddr_set(uint8_t ddr) {
    MFP68901_R(MFP_DDR) = ddr;
}

static inline void mfp68901_ddr_set_masked(uint8_t ddr, uint8_t mask) {
    MFP68901_R(MFP_DDR) = (MFP68901_R(MFP_DDR) & ~mask) | (ddr & mask);
}

static inline void mfp68901_gpip_config(uint8_t gpdr, uint8_t aer, uint8_t ddr) {
    MFP68901_R(MFP_DDR) &= ddr;
    MFP68901_R(MFP_AER) = aer;
    MFP68901_R(MFP_GPDR) = gpdr;
    MFP68901_R(MFP_DDR) |= ddr;
}

static inline void mfp68901_gpip_config_masked(uint8_t gpdr, uint8_t aer, uint8_t ddr, uint8_t mask) {
    MFP68901_R(MFP_DDR) &= (~mask) | (ddr & mask);
    MFP68901_R(MFP_AER) = (MFP68901_R(MFP_AER) & ~mask) | (aer & mask);
    MFP68901_R(MFP_GPDR) = (MFP68901_R(MFP_GPDR) & ~mask) | (gpdr & mask);
    MFP68901_R(MFP_DDR) |= (ddr & mask);
}

////////////////////////////////////////////////////////////////////////////////
// TIMERS                                                                     //
////////////////////////////////////////////////////////////////////////////////

enum mfp68901_tacr {
    MFP68901_TACR_AC_STOPPED    = 0x00,     // A Control: Timer Stopped
    MFP68901_TACR_AC_DELAY_4    = 0x01,     // A Control: Delay Mode, /4 Prescaler
    MFP68901_TACR_AC_DELAY_10   = 0x02,     // A Control: Delay Mode, /10 Prescaler
    MFP68901_TACR_AC_DELAY_16   = 0x03,     // A Control: Delay Mode, /16 Prescaler
    MFP68901_TACR_AC_DELAY_50   = 0x04,     // A Control: Delay Mode, /50 Prescaler
    MFP68901_TACR_AC_DELAY_64   = 0x05,     // A Control: Delay Mode, /64 Prescaler
    MFP68901_TACR_AC_DELAY_100  = 0x06,     // A Control: Delay Mode, /100 Prescaler
    MFP68901_TACR_AC_DELAY_200  = 0x07,     // A Control: Delay Mode, /200 Prescaler
    MFP68901_TACR_AC_EVENT      = 0x08,     // A Control: Event Count Mode
    MFP68901_TACR_AC_PULSE_4    = 0x09,     // A Control: Pulse Width Mode, /4 Prescaler
    MFP68901_TACR_AC_PULSE_10   = 0x0A,     // A Control: Pulse Width Mode, /10 Prescaler
    MFP68901_TACR_AC_PULSE_16   = 0x0B,     // A Control: Pulse Width Mode, /16 Prescaler
    MFP68901_TACR_AC_PULSE_50   = 0x0C,     // A Control: Pulse Width Mode, /50 Prescaler
    MFP68901_TACR_AC_PULSE_64   = 0x0D,     // A Control: Pulse Width Mode, /64 Prescaler
    MFP68901_TACR_AC_PULSE_100  = 0x0E,     // A Control: Pulse Width Mode, /100 Prescaler
    MFP68901_TACR_AC_PULSE_200  = 0x0F,     // A Control: Pulse Width Mode, /200 Prescaler
    MFP68901_TACR_AC_MASK       = 0x0F,

    MFP68901_TACR_RESET_TAO     = 0x10,     // Reset TAO

    MFP68901_TACR_MASK          = 0x1F,
    MFP68901_TACR_MAX           = 0x1F,
};

enum mfp68901_tbcr {
    MFP68901_TBCR_BC_STOPPED    = 0x00,     // B Control: Timer Stopped
    MFP68901_TBCR_BC_DELAY_4    = 0x01,     // B Control: Delay Mode, /4 Prescaler
    MFP68901_TBCR_BC_DELAY_10   = 0x02,     // B Control: Delay Mode, /10 Prescaler
    MFP68901_TBCR_BC_DELAY_16   = 0x03,     // B Control: Delay Mode, /16 Prescaler
    MFP68901_TBCR_BC_DELAY_50   = 0x04,     // B Control: Delay Mode, /50 Prescaler
    MFP68901_TBCR_BC_DELAY_64   = 0x05,     // B Control: Delay Mode, /64 Prescaler
    MFP68901_TBCR_BC_DELAY_100  = 0x06,     // B Control: Delay Mode, /100 Prescaler
    MFP68901_TBCR_BC_DELAY_200  = 0x07,     // B Control: Delay Mode, /200 Prescaler
    MFP68901_TBCR_BC_EVENT      = 0x08,     // B Control: Event Count Mode
    MFP68901_TBCR_BC_PULSE_4    = 0x09,     // B Control: Pulse Width Mode, /4 Prescaler
    MFP68901_TBCR_BC_PULSE_10   = 0x0A,     // B Control: Pulse Width Mode, /10 Prescaler
    MFP68901_TBCR_BC_PULSE_16   = 0x0B,     // B Control: Pulse Width Mode, /16 Prescaler
    MFP68901_TBCR_BC_PULSE_50   = 0x0C,     // B Control: Pulse Width Mode, /50 Prescaler
    MFP68901_TBCR_BC_PULSE_64   = 0x0D,     // B Control: Pulse Width Mode, /64 Prescaler
    MFP68901_TBCR_BC_PULSE_100  = 0x0E,     // B Control: Pulse Width Mode, /100 Prescaler
    MFP68901_TBCR_BC_PULSE_200  = 0x0F,     // B Control: Pulse Width Mode, /200 Prescaler
    MFP68901_TBCR_BC_MASK       = 0x0F,

    MFP68901_TBCR_RESET_TBO     = 0x10,     // Reset TBO

    MFP68901_TBCR_MASK          = 0x1F,
    MFP68901_TBCR_MAX           = 0x1F,
};

enum mfp68901_tcdcr {
    MFP68901_TCDCR_CC_STOPPED   = 0x0 << 4, // C Control: Timer Stopped
    MFP68901_TCDCR_CC_DELAY_4   = 0x1 << 4, // C Control: Delay Mode, /4 Prescaler
    MFP68901_TCDCR_CC_DELAY_10  = 0x2 << 4, // C Control: Delay Mode, /10 Prescaler
    MFP68901_TCDCR_CC_DELAY_16  = 0x3 << 4, // C Control: Delay Mode, /16 Prescaler
    MFP68901_TCDCR_CC_DELAY_50  = 0x4 << 4, // C Control: Delay Mode, /50 Prescaler
    MFP68901_TCDCR_CC_DELAY_64  = 0x5 << 4, // C Control: Delay Mode, /64 Prescaler
    MFP68901_TCDCR_CC_DELAY_100 = 0x6 << 4, // C Control: Delay Mode, /100 Prescaler
    MFP68901_TCDCR_CC_DELAY_200 = 0x7 << 4, // C Control: Delay Mode, /200 Prescaler
    MFP68901_TCDCR_CC_MASK      = 0x7 << 4,

    MFP68901_TCDCR_DC_STOPPED   = 0x0 << 0, // D Control: Timer Stopped
    MFP68901_TCDCR_DC_DELAY_4   = 0x1 << 0, // D Control: Delay Mode, /4 Prescaler
    MFP68901_TCDCR_DC_DELAY_10  = 0x2 << 0, // D Control: Delay Mode, /10 Prescaler
    MFP68901_TCDCR_DC_DELAY_16  = 0x3 << 0, // D Control: Delay Mode, /16 Prescaler
    MFP68901_TCDCR_DC_DELAY_50  = 0x4 << 0, // D Control: Delay Mode, /50 Prescaler
    MFP68901_TCDCR_DC_DELAY_64  = 0x5 << 0, // D Control: Delay Mode, /64 Prescaler
    MFP68901_TCDCR_DC_DELAY_100 = 0x6 << 0, // D Control: Delay Mode, /100 Prescaler
    MFP68901_TCDCR_DC_DELAY_200 = 0x7 << 0, // D Control: Delay Mode, /200 Prescaler
    MFP68901_TCDCR_DC_MASK      = 0x7 << 0,

    MFP68901_TCDCR_MASK         = 0x77,
    MFP68901_TCDCR_MAX          = 0x77,
};

static inline void mfp68901_tacr_reset_tao(void) {
    MFP68901_R(MFP_TACR) |= MFP68901_TACR_RESET_TAO;
}

static inline enum mfp68901_tacr mfp68901_tacr_ac_get(void) {
    return MFP68901_R(MFP_TACR) & MFP68901_TACR_AC_MASK;
}

static inline void mfp68901_tacr_ac_set(enum mfp68901_tacr tacr) {
    MFP68901_R(MFP_TACR) = (MFP68901_R(MFP_TACR) & ~MFP68901_TACR_AC_MASK) | (tacr & MFP68901_TACR_AC_MASK);
}

static inline void mfp68901_tbcr_reset_tbo(void) {
    MFP68901_R(MFP_TBCR) |= MFP68901_TBCR_RESET_TBO;
}

static inline enum mfp68901_tbcr mfp68901_tbcr_bc_get(void) {
    return MFP68901_R(MFP_TBCR) & MFP68901_TBCR_BC_MASK;
}

static inline void mfp68901_tbcr_bc_set(enum mfp68901_tbcr tbcr) {
    MFP68901_R(MFP_TBCR) = (MFP68901_R(MFP_TBCR) & ~MFP68901_TBCR_BC_MASK) | (tbcr & MFP68901_TBCR_BC_MASK);
}

static inline enum mfp68901_tcdcr mfp68901_tcdcr_cc_get(void) {
    return MFP68901_R(MFP_TCDCR) & MFP68901_TCDCR_CC_MASK;
}

static inline void mfp68901_tcdcr_cc_set(enum mfp68901_tcdcr tcdcr) {
    MFP68901_R(MFP_TCDCR) = (MFP68901_R(MFP_TCDCR) & ~MFP68901_TCDCR_CC_MASK) | (tcdcr & MFP68901_TCDCR_CC_MASK);
}

static inline enum mfp68901_tcdcr mfp68901_tcdcr_dc_get(void) {
    return MFP68901_R(MFP_TCDCR) & MFP68901_TCDCR_DC_MASK;
}

static inline void mfp68901_tcdcr_dc_set(enum mfp68901_tcdcr tcdcr) {
    MFP68901_R(MFP_TCDCR) = (MFP68901_R(MFP_TCDCR) & ~MFP68901_TCDCR_DC_MASK) | (tcdcr & MFP68901_TCDCR_DC_MASK);
}

static inline uint8_t mfp68901_tadr_get(void) {
    return MFP68901_R(MFP_TADR);
}

static inline void mfp68901_tadr_set(uint8_t tadr) {
    MFP68901_R(MFP_TADR) = tadr;
}

static inline uint8_t mfp68901_tbdr_get(void) {
    return MFP68901_R(MFP_TBDR);
}

static inline void mfp68901_tbdr_set(uint8_t tbdr) {
    MFP68901_R(MFP_TBDR) = tbdr;
}

static inline uint8_t mfp68901_tcdr_get(void) {
    return MFP68901_R(MFP_TCDR);
}

static inline void mfp68901_tcdr_set(uint8_t tcdr) {
    MFP68901_R(MFP_TCDR) = tcdr;
}

static inline uint8_t mfp68901_tddr_get(void) {
    return MFP68901_R(MFP_TDDR);
}

static inline void mfp68901_tddr_set(uint8_t tddr) {
    MFP68901_R(MFP_TDDR) = tddr;
}

////////////////////////////////////////////////////////////////////////////////
// UNIVERSAL SYNCHRONOUS/ASYNCHRONOUS RECEIVER-TRANSMITTER                    //
////////////////////////////////////////////////////////////////////////////////

enum mfp68901_ucr {
    MFP68901_UCR_CLK_1              = 0 << 7,   // Clock Mode: Divide-by-1
    MFP68901_UCR_CLK_16             = 1 << 7,   // Clock Mode: Divide-by-16
    MFP68901_UCR_CLK_MASK           = 1 << 7,

    MFP68901_UCR_WL_8               = 0 << 5,   // Word Length: 8 Bits
    MFP68901_UCR_WL_7               = 1 << 5,   // Word Length: 7 Bits
    MFP68901_UCR_WL_6               = 2 << 5,   // Word Length: 6 Bits
    MFP68901_UCR_WL_5               = 3 << 5,   // Word Length: 5 Bits
    MFP68901_UCR_WL_MASK            = 3 << 5,

    MFP68901_UCR_ST_0_0__SYNC       = 0 << 3,   // Start/Stop Bit and Format Control: 0 Start, 0 Stop, Synchronous
    MFP68901_UCR_ST_1_1__ASYNC      = 1 << 3,   // Start/Stop Bit and Format Control: 1 Start, 1 Stop, Asynchronous
    MFP68901_UCR_ST_1_15_ASYNC      = 2 << 3,   // Start/Stop Bit and Format Control: 1 Start, 1.5 Stop, Asynchronous
    MFP68901_UCR_ST_1_2__ASYNC      = 3 << 3,   // Start/Stop Bit and Format Control: 1 Start, 2 Stop, Asynchronous
    MFP68901_UCR_ST_MASK            = 3 << 3,

    MFP68901_UCR_PE                 = 1 << 2,   // Parity Enable

    MFP68901_UCR_EO_ODD             = 0 << 1,   // Even/Odd Parity: Odd
    MFP68901_UCR_EO_EVEN            = 1 << 1,   // Even/Odd Parity: Even
    MFP68901_UCR_EO_MASK            = 1 << 1,

    MFP68901_UCR_MASK               = 0xFE,
    MFP68901_UCR_MAX                = 0xFE,
};

enum mfp68901_rsr {
    MFP68901_RSR_BF                 = 1 << 7,   // Buffer Full
    MFP68901_RSR_OE                 = 1 << 6,   // Overrun Error
    MFP68901_RSR_PE                 = 1 << 5,   // Parity Error
    MFP68901_RSR_FE                 = 1 << 4,   // Frame Error
    MFP68901_RSR_FS                 = 1 << 3,   // Found/Search
    MFP68901_RSR_B                  = 1 << 3,   // Break Detect
    MFP68901_RSR_M                  = 1 << 2,   // Match
    MFP68901_RSR_CIP                = 1 << 2,   // Character in Progress
    MFP68901_RSR_SS                 = 1 << 1,   // Synchronous Strip Enable
    MFP68901_RSR_RE                 = 1 << 0,   // Receiver Enable

    MFP68901_RSR_MASK               = 0xFF,
    MFP68901_RSR_MAX                = 0xFF,
};

enum mfp68901_tsr {
    MFP68901_TSR_BE                 = 1 << 7,   // Buffer Empty
    MFP68901_TSR_UE                 = 1 << 6,   // Underrun Error
    MFP68901_TSR_AT                 = 1 << 5,   // Auto-Turnaround
    MFP68901_TSR_END                = 1 << 4,   // End of Transmission
    MFP68901_TSR_B                  = 1 << 3,   // Break

    MFP68901_TSR_HL_HIGH_IMPEDANCE  = 0 << 1,   // High and Low: High Impedance
    MFP68901_TSR_HL_LOW             = 1 << 1,   // High and Low: Low
    MFP68901_TSR_HL_HIGH            = 2 << 1,   // High and Low: High
    MFP68901_TSR_HL_LOOPBACK_MODE   = 3 << 1,   // High and Low: Loopback Mode
    MFP68901_TSR_HL_MASK            = 3 << 1,

    MFP68901_TSR_TE                 = 1 << 0,   // Transmitter Enable

    MFP68901_TSR_MASK               = 0xFF,
    MFP68901_TSR_MAX                = 0xFF,
};

static inline uint8_t mfp68901_scr_get(void) {
    return MFP68901_R(MFP_SCR);
}

static inline void mfp68901_scr_set(uint8_t scr) {
    MFP68901_R(MFP_SCR) = scr;
}

static inline enum mfp68901_ucr mfp68901_ucr_get(void) {
    return MFP68901_R(MFP_UCR);
}

static inline void mfp68901_ucr_set(enum mfp68901_ucr ucr) {
    MFP68901_R(MFP_UCR) = ucr;
}

static inline enum mfp68901_rsr mfp68901_rsr_get() {
    return MFP68901_R(MFP_RSR);
}

static inline void mfp68901_rsr_set(enum mfp68901_rsr rsr) {
    MFP68901_R(MFP_RSR) = rsr;
}

static inline enum mfp68901_tsr mfp68901_tsr_get() {
    return MFP68901_R(MFP_TSR);
}

static inline void mfp68901_tsr_set(enum mfp68901_tsr tsr) {
    MFP68901_R(MFP_TSR) = tsr;
}

static inline uint8_t mfp68901_udr_get() {
    return MFP68901_R(MFP_UDR);
}

static inline void mfp68901_udr_set(uint8_t udr) {
    MFP68901_R(MFP_UDR) = udr;
}

#undef MFP68901_R

#endif
