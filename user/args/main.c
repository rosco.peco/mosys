#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <spawn.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <mosys/spi/spidev.h>
#include <mosys/spi/at25.h>

int main(int argc, char **argv, char **envp) {
    printf("argc: %d\n", argc);
    printf("argv (%p):\n", (void *) argv);
    for (char **argv_ptr = argv; *argv_ptr; argv_ptr += 1) {
        printf("    \"%s\"\n", *argv_ptr);
    }
    printf("envp (%p):\n", (void *) envp);
    for (char **envp_ptr = envp; *envp_ptr; envp_ptr += 1) {
        printf("    \"%s\"\n", *envp_ptr);
    }

    int dev_at25 = open("/dev/at250", O_RDWR);
    if (dev_at25 < 0) {
        err(EXIT_FAILURE, "Failed to open(/dev/at250)");
    }

    ssize_t result;

    uint8_t bp = AT25_BP_NONE;
    result = ioctl(dev_at25, AT25_IOC_WR_BP, &bp);
    if (result < 0) {
        err(EXIT_FAILURE, "Failed to ioctl(/dev/at250)");
    }

    result = ioctl(dev_at25, AT25_IOC_RD_BP, &bp);
    if (result < 0) {
        err(EXIT_FAILURE, "Failed to ioctl(/dev/at250)");
    }
    printf("bp: %hhu\n", bp);

    short tmp = 0x4a4a;
    result = write(dev_at25, &tmp, sizeof(tmp));
    if (result < 0) {
        err(EXIT_FAILURE, "Failed to write(/dev/at250)");
    }

    close(dev_at25);

    dev_at25 = open("/dev/at250", O_RDWR);
    if (dev_at25 < 0) {
        err(EXIT_FAILURE, "Failed to open(/dev/at250)");
    }

    char buf[64];
    result = read(dev_at25, buf, sizeof(buf));
    if (result < 0) {
        err(EXIT_FAILURE, "Failed to read(/dev/at250)");
    }
    for (int i = 0; i < sizeof(buf); ++i) {
        printf("%02hhx ", buf[i]);
        if (i % 16 == 15) {
            printf("\n");
        }
    }

    return EXIT_SUCCESS; 
}
