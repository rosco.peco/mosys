#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <sched.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <spawn.h>
#include <sys/syscall.h>
#include <sys/wait.h>

#define EXIT_FAILURE_PID            2
#define EXIT_FAILURE_TEST_SYSCALLS  3
#define EXIT_FAILURE_STDIO_OPEN     4

bool test_syscalls(void) {
    bool pass = true;

    errno = 0;
    long succeed_result = syscall(SYS_succeed);
    if (succeed_result != 0) {
        pass = false;
    }

    errno = 0;
    long fail_value = 0x3078544A;
    long fail_result = syscall(SYS_fail, fail_value);
    if (fail_result != -1 || errno != fail_value) {
        pass = false;
    }

    return pass;
}

int main() {
    pid_t pid = getpid();
    if (pid != 1) {
        exit(EXIT_FAILURE_PID);
    }
 
    if (!test_syscalls()) {
        exit(EXIT_FAILURE_TEST_SYSCALLS);
    }

    int stdin_fd = open("/dev/uart0", O_RDONLY);
    int stdout_fd = open("/dev/uart0", O_WRONLY);
    int stderr_fd = open("/dev/uart0", O_WRONLY);

    if (stdin_fd != 0   ||
        stdout_fd != 1  ||
        stderr_fd != 2) {
        exit(EXIT_FAILURE_STDIO_OPEN);
    }

    printf("Started `init`\n");

    char *spawn_argv[] = {
        "sh.elf",
        "--startedby=init.elf",
        NULL
    };
    char *spawn_envp[] = {
        "PATH=",
        NULL
    };
    posix_spawn(NULL, "/sh.elf", NULL, NULL, spawn_argv, spawn_envp);

    while (1) {
        pid_t waited_pid = wait(NULL);
        if (waited_pid == -1 && errno == ECHILD) {
            // When init has no children left
            printf("All other processes have terminated, `init` exiting\n");
            exit(EXIT_SUCCESS);
        }
    }
}
