#include <unistd.h>

#include <time.h>

unsigned sleep(unsigned seconds) {
    struct timespec req;
    req.tv_sec = seconds;
    req.tv_nsec = 0;
    
    struct timespec rem;

    if (nanosleep(&req, &rem) == 0) {
        return 0;
    } else {
        return rem.tv_sec + (rem.tv_nsec > 0 ? 1 : 0);
    }
}
