#include <unistd.h>

#include <sys/syscall.h>

_Noreturn void _exit(int status) {
    syscall(SYS__exit, status);
    while (1) {
        // Satisfy compiler
    }
}
