    import  _errno
    export  syscall
    
    section .text

; long syscall(long number, ...);
syscall:
    link    A6,#0
    
    ; Save syscall-used non-volatile registers
    movem.l D2-D5,-(SP)

    ; Load function arguments to D0-D5/A0
    movea.l A6,A1
    addq.l  #8,A1   ; Skip saved frame pointer and return address in frame
    movem.l (A1)+,D0-D5/A0

    ; Trap to kernel
    trap    #0
    
    ; Restore saved non-volatile registers
    movem.l (SP)+,D2-D5

    ; Set _errno to -D0 and set D0 to -1 if D0 is negative
    tst.l   D0
    bpl     .no_error
    neg.l   D0
    move.l  D0,(_errno)  ; size must match sizeof(_errno)
    moveq.l #-1,D0
.no_error:

    unlk    A6
    rts
