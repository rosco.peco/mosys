#include <err.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern char *__progname;

// TODO: Print to stderr instead of stdout

void err(int eval, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  verr(eval, fmt, args);
  va_end(args);
}

void errx(int eval, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  verr(eval, fmt, args);
  va_end(args);
}

void warn(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  vwarn(fmt, args);
  va_end(args);
}

void warnx(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  vwarnx(fmt, args);
  va_end(args);
}

void verr(int eval, const char *fmt, va_list args) {
    vwarn(fmt, args);
    exit(eval);
}

void verrx(int eval, const char *fmt, va_list args) {
    vwarnx(fmt, args);
    exit(eval);
}

void vwarn(const char *fmt, va_list args) {
    printf("%s: ", __progname);
    vprintf(fmt, args);
    const char *sep = fmt ? ": " : "";
    // TODO: Enable printing strerror(errno) instead of dummy
    // printf("%s%s", sep, strerror(errno));
    printf("%s`strerrno(%d)`", sep, errno);
    printf("\n");
}

void vwarnx(const char *fmt, va_list args) {
    printf("%s: ", __progname);
    vprintf(fmt, args);
    printf("\n");
}
