#include <spawn.h>

#include <sched.h>
#include <stddef.h>
#include <unistd.h>

struct spawn_args {
    const char *path;
    const posix_spawn_file_actions_t *file_actions;
    const posix_spawnattr_t *attrp;
    char *const *argv;
    char *const *envp;
};

static int spawn_pre_exec(void *arg);
static int spawn_exec(struct spawn_args *args);

static int spawn_pre_exec(void *arg) {
    struct spawn_args *args = arg;

    // TODO: Perform housekeeping actions

    return spawn_exec(args);
}

static int spawn_exec(struct spawn_args *args) {
    execve(args->path, args->argv, args->envp);
    return 127; // Specified return value for failing
}

int posix_spawn(pid_t *pid, const char *path,
                const posix_spawn_file_actions_t *file_actions,
                const posix_spawnattr_t *attrp,
                char *const argv[], char *const envp[]) {
    struct spawn_args args = {
        path,
        file_actions,
        attrp,
        argv,
        envp
    };

    int clone_result = clone(spawn_pre_exec, NULL, CLONE_VM | CLONE_VFORK, &args);

    if (clone_result >= 0) {
        if (pid) {
            *pid = clone_result;
        }
        return 0;
    } else {
        return clone_result;
    }
}
