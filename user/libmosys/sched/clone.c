#include <sched.h>

#include <stdlib.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>

int clone(int (*fn)(void *), void *stack, int flags, void *arg, ...
          /* pid_t *parent_tid, void *tls, pid_t *child_tid */) {
    int status = syscall(SYS_clone, flags, stack);
    if (status != 0) {
        return status;
    }

    int fn_exit = fn(arg);

    exit(fn_exit);
}
