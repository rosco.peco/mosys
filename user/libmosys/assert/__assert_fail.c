#include <assert.h>

#include <err.h>
#include <stdlib.h>

void __assert_fail(const char *__expression, const char *__file, unsigned long __line, const char *__func) {
    warnx("%s:%lu: %s: Assertion `%s' failed.", __file, __line, __func, __expression);
    // TODO: assert() should abort(), not exit()
    // abort();
    exit(EXIT_FAILURE);
}
