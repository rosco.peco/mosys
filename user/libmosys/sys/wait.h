#ifndef _INCLUDE_SYS_WAIT_H
#define _INCLUDE_SYS_WAIT_H

#include <sys/types.h>

#define WCONTINUED  0x2
#define WNOHANG     0x1
#define WUNTRACED   0x4

pid_t wait(int *wstatus);
pid_t waitpid(pid_t pid, int *wstatus, int options);

#endif
