#ifndef _INCLUDE_SYS_SYSCALL_H
#define _INCLUDE_SYS_SYSCALL_H

#define SYS_succeed     0
#define SYS_fail        1
#define SYS_getpid      2
#define SYS_getppid     3
#define SYS_open        4
#define SYS_close       5
#define SYS_read        6
#define SYS_write       7
#define SYS_nanosleep   8
#define SYS_clone       9
#define SYS_execve      10
#define SYS__exit       11
#define SYS_waitpid     12
#define SYS_getdents    13
#define SYS_ioctl       14

#define SYS_do_trace    0x80000000

#include <dirent.h>

static ssize_t getdents(int fd, struct dirent *drnt, size_t count) {
    return syscall(SYS_getdents, fd, drnt, count);
}

#endif
