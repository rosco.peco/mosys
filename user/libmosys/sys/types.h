#ifndef _INCLUDE_SYS_TYPES_H
#define _INCLUDE_SYS_TYPES_H

#ifndef _INO_T
typedef unsigned long ino_t;
#endif

#ifndef _OFF_T
typedef long off_t;
#endif

#ifndef _PID_T
typedef int pid_t;
#endif

#ifndef _SSIZE_T
typedef long ssize_t;
#endif

#ifndef _TIME_T
typedef unsigned long time_t;
#endif

#endif
