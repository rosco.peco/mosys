#ifndef _INCLUDE_STDLIB_H
#define _INCLUDE_STDLIB_H

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif

#ifndef NULL
#define NULL ((void *) 0)
#endif

#ifndef _SIZE_T
#define _SIZE_T
typedef unsigned long int size_t;
#endif

_Noreturn void _Exit(int status);
_Noreturn void exit(int status);

#endif
