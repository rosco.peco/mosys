#ifndef _INCLUDE_SCHED_H
#define _INCLUDE_SCHED_H

#define CLONE_VM        0x0001U
#define CLONE_VFORK     0x0002U

int clone(int (*fn)(void *), void *stack, int flags, void *arg, ...
          /* pid_t *parent_tid, void *tls, pid_t *child_tid */);

#endif
