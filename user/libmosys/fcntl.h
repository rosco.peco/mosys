#ifndef _INCLUDE_FCNTL_H
#define _INCLUDE_FCNTL_H

#define O_RDONLY    (0x0001)
#define O_WRONLY    (0x0002)
#define O_RDWR      (O_RDONLY | O_WRONLY)

int open(const char *path, int oflag, ...);

#endif
