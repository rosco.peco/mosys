#ifndef _INCLUDE_ERRNO_H
#define _INCLUDE_ERRNO_H

extern int _errno;
#define errno _errno

#define EOK     0
#define EPERM   1
#define ENOENT  2
#define EINVAL  3
#define ENOSYS  4
#define EBADF   5
#define EFAULT  6
#define ENOSPC  7
#define ENOMEM  8
#define ENOEXEC 9
#define ECHILD  10
#define ENOTDIR 11
#define ENOTTY  12

#endif
