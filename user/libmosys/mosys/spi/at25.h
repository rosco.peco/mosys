#ifndef _INCLUDE_MOSYS_SPI_AT25_H
#define _INCLUDE_MOSYS_SPI_AT25_H

#include <asm/ioctl.h>
#include <stdint.h>

#define AT25_BP_NONE            0
#define AT25_BP_UPPER_QUARTER   1
#define AT25_BP_UPPER_HALF      2
#define AT25_BP_UPPER_ALL       3

#define AT25_IOC_MAGIC 'q'

// Write-Protect Enable
#define AT25_IOC_RD_WPEN    _IOR(AT25_IOC_MAGIC, 0, bool)
#define AT25_IOC_WR_WPEN    _IOW(AT25_IOC_MAGIC, 0, bool)

// Block Protection
#define AT25_IOC_RD_BP      _IOR(AT25_IOC_MAGIC, 1, uint8_t)
#define AT25_IOC_WR_BP      _IOW(AT25_IOC_MAGIC, 1, uint8_t)

#endif
