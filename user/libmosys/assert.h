#ifndef _INCLUDE_ASSERT_H
#define _INCLUDE_ASSERT_H

#ifdef NDEBUG
#define assert(ignore) ((void) 0)
#else
void __assert_fail(const char *__expression, const char *__file, unsigned long __line, const char *__func);
#define assert(expression) (((expression) ? (void) 0 : __assert_fail(#expression, __FILE__, __LINE__, __func__)))
#endif

#define static_assert _Static_assert

#endif
