    export  _start
    import  main
    import  exit
    import __progname

    section .text

_start:
    ; Push envp, argv, argc and call main()
    move.l  A1,-(SP)
    move.l  A0,-(SP)
    move.l  D0,-(SP)

    ; Store argv[0] to __progname
    move.l  (0,A0),(__progname)

    jsr     main
    add.l   #12,SP

    ; Push main return value and call exit()
    move.l  D0,-(SP)
    jsr     exit
    add.l   #4,SP
