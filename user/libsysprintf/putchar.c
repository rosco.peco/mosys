#include <unistd.h>

void _putchar(char c) {
    if (c == '\n') {
        char tmp_cr = '\r';
        write(1, &tmp_cr, 1);
    }

    write(1, &c, 1);
}
