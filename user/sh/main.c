#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <spawn.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/wait.h>

// The following three functions are inspired by the GLibC implementations

char *strpbrk(const char *s, const char *accept) {
    for (; *s != '\0'; ++s) {
        for (const char *a = accept; *a != '\0'; ++a) {
            if (*a == *s)
                return (char *) s;
        }
    }

    return NULL;
}

size_t strspn(const char *s, const char *accept) {
    size_t count = 0;

    for (; *s != '\0'; ++s) {
        const char *a;
        for (a = accept; *a != '\0'; ++a) {
            if (*s == *a)
                break;
        }
        if (*a == '\0') {
            return count;
        } else {
            count += 1;
        }
    }

    return count;
}

char *strtok(char *str, const char *delim) {
    static char *oldstr;
    char *token;

    if (!str) {
        str = oldstr;
    }

    str += strspn(str, delim);
    if (*str == '\0') {
        oldstr = str;
        return NULL;
    }

    token = str;
    str = strpbrk(token, delim);
    if (str == NULL) {
        oldstr = token + strlen(token);
    } else {
        *str = '\0';
        oldstr = str + 1;
    }

    return token;
}

ssize_t get_line(size_t line_len, char line[line_len]) {
    size_t line_index = 0;

    while (1) {
        ssize_t status = read(STDIN_FILENO, &line[line_index], 1);

        if (status > 0) {
            write(STDOUT_FILENO, &line[line_index], 1);

            if (line[line_index] == '\n') {
                // Done on newline
                line[line_index] = '\0';
                return line_index;
            } else if (line[line_index] == '\r') {
                // Ignore carriage return
                line[line_index] = '\0';
                continue;
            }

            line_index += status;
            line[line_index] = '\0';
        }
    }
}

int main(int argc, char **argv, char **envp) {
    while (1) {
        printf("# ", (int) getpid());

        char buf[80];
        get_line(sizeof(buf), buf);

        char *spawn_argv[8] = {0};

        char *tok_ptr = buf;
        for (unsigned i = 0; i < sizeof(spawn_argv) / sizeof(spawn_argv[0]); ++i) {
            spawn_argv[i] = strtok(tok_ptr, " \t");
            tok_ptr = NULL;

            if (!spawn_argv[i])
                break;
        }


        if (strcmp(buf, "exit") == 0) {
            exit(EXIT_SUCCESS);
        } else {
            posix_spawn(NULL, spawn_argv[0], NULL, NULL, spawn_argv, envp);
            int wstatus;
            int wait_result = wait(&wstatus);
            if (wait_result > 0) {
                if (wstatus == 127) {
                    printf("Failed to execute \"%s\"\n", buf);
                }
            }
        }
    }
}
