#include "ownedmem.h"
#include "panic.h"

ownedmem *ownedmem_new_from_ptr_range(ptr_range range) {
    ownedmem *owndmm = kalloc(sizeof(ownedmem));

    if (owndmm) {
        mutex_init(&owndmm->mtx);
        owndmm->ref_count = 1;
        owndmm->range = range;
    }

    return owndmm;
}

ownedmem *ownedmem_new(size_t size) {
    ptr_range range = kalloc_range(size);

    ownedmem *owndmm = NULL;
    if (range.start) {
        owndmm = ownedmem_new_from_ptr_range(range);
        if (!owndmm) {
            kfree(range.start);
        }
    }
    
    return owndmm;
}

void ownedmem_delete(ownedmem *owndmm) {
    if (!owndmm) {
        return;
    }

    if (owndmm->ref_count != 0) {
        panic("Tried to delete still-referenced ownedmem");
    }

    if (owndmm->range.start) {
        kfree(owndmm->range.start);
        owndmm->range = PTR_RANGE_NULL;
    }

    kfree(owndmm);
}

ownedmem *ownedmem_ref(ownedmem *owndmm) {
    if (!owndmm) {
        return NULL;
    }

    mutex_lock(&owndmm->mtx);

    if (owndmm->ref_count == SIZE_MAX) {
        panic("Overflowed ownedmem ref_count");
    }
    owndmm->ref_count += 1;

    mutex_unlock(&owndmm->mtx);
    return owndmm;
}

void ownedmem_unref(ownedmem *owndmm) {
    if (!owndmm) {
        return;
    }

    mutex_lock(&owndmm->mtx);

    if (owndmm->ref_count == 0) {
        panic("Underflowed ownedmem ref_count");
    }
    owndmm->ref_count -= 1;

    if (owndmm->ref_count == 0) {
        ownedmem_delete(owndmm);
        owndmm = NULL;  // owndmm is no longer valid
    } else {
        mutex_unlock(&owndmm->mtx);
    }
}

ownedmem *ownedmem_clone(ownedmem *owndmm_orig) {
    if (!owndmm_orig) {
        return NULL;
    }

    mutex_lock(&owndmm_orig->mtx);
    ptr_range cloned_range = kalloc_clone(owndmm_orig->range);
    mutex_unlock(&owndmm_orig->mtx);

    ownedmem *owndmm = NULL;
    if (cloned_range.start) {
        owndmm = ownedmem_new_from_ptr_range(cloned_range);
    }

    return owndmm;
}

ownedmem *ownedmem_clone_starting_at(ownedmem *owndmm_orig, void *starting_at) {
    if (!owndmm_orig) {
        return NULL;
    }

    mutex_lock(&owndmm_orig->mtx);
    ptr_range cloned_range = kalloc_clone_starting_at(owndmm_orig->range, starting_at);
    mutex_unlock(&owndmm_orig->mtx);

    ownedmem *owndmm = NULL;
    if (cloned_range.start) {
        owndmm = ownedmem_new_from_ptr_range(cloned_range);
    }

    return owndmm;
}

ownedmem *ownedmem_clone_ending_at(ownedmem *owndmm_orig, void *ending_at) {
    if (!owndmm_orig) {
        return NULL;
    }

    mutex_lock(&owndmm_orig->mtx);
    ptr_range cloned_range = kalloc_clone_ending_at(owndmm_orig->range, ending_at);
    mutex_unlock(&owndmm_orig->mtx);

    ownedmem *owndmm = NULL;
    if (cloned_range.start) {
        owndmm = ownedmem_new_from_ptr_range(cloned_range);
    }

    return owndmm;
}
