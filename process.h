#ifndef INCLUDE_PROCESS_H
#define INCLUDE_PROCESS_H

#include "syscall.h"
#include "thread.h"
#include "kalloc.h"
#include "ownedmem.h"
#include "vfs.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdnoreturn.h>

#define CLONE_VM        0x0001U
#define CLONE_VFORK     0x0002U

typedef int pid_t;

typedef struct process_list {
    struct process *head;
    struct process *tail;
    spinlock lock;
} process_list;

typedef struct process {
    struct thread *owned_thread;
    struct process *parent;
    pid_t pid;
    ownedmem *user_space;
    ownedmem *arg_env_space;
    fdesc_list *fdl;
    process_list children;
    struct process *next;   // Used for child list
} process;

typedef void (*process_entry)();

process *process_new(void);
void process_delete(process *process_ptr);

// List must not already be locked
void process_list_enqueue(process_list *list, process *enq_process);
process *process_list_dequeue(process_list *list);
// List must already be locked
bool process_list_remove(process_list *list, process *enq_process);

void process_setup(process *process_ptr, void (*func)(), ownedmem *sup_stack, ownedmem *usr_stack);

noreturn void process_enter_sup(void *ssp, void *usp, process_entry pc, uint16_t sr);
noreturn void process_enter_usr(void *ssp, void *usp, process_entry pc, uint16_t sr);
noreturn void process_enter(void *ssp, void *usp, process_entry pc, uint16_t sr);
struct syscall_state *process_enter_setup(void *ssp, void *usp, process_entry pc, uint16_t sr);

noreturn void process_enter_state(struct syscall_state *ssp);

process *process_clone(unsigned long flags, void *stack,
                    //    int *parent_tid, int *child_tid,
                    //    unsigned long tls,
                       int *errno_ptr);

int process_execve(size_t pathname_len, const char pathname[pathname_len],
                   size_t argv_len, char *const argv[argv_len],
                   size_t envp_len, char *const envp[envp_len],
                   int *errno_ptr);

noreturn void process_exit_thread(int status);

pid_t process_waitpid(pid_t pid, int *wstatus, int options, int *errno_ptr);

#endif
