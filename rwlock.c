#include "rwlock.h"
#include <string.h>

void rwlock_init(rwlock *rwl) {
    const static rwlock tmp =  RWLOCK_INIT;
    memcpy(rwl , &tmp, sizeof(rwl));
}

void rwlock_read_begin(rwlock *rwl) {
    mutex_lock(&rwl->r);
    if (rwl->b++ == 0) {
        mutex_lock(&rwl->g);
    }
    mutex_unlock(&rwl->r);
}

void rwlock_read_end(rwlock *rwl) {
    mutex_lock(&rwl->r);
    if (--rwl->b == 0) {
        mutex_unlock(&rwl->g);
    }
    mutex_unlock(&rwl->r);
}

void rwlock_write_begin(rwlock *rwl) {
    mutex_lock(&rwl->g);
}

void rwlock_write_end(rwlock *rwl) {
    mutex_unlock(&rwl->g);
}
