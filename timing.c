#include "timing.h"
#include "thread.h"

static unsigned long long uptime_us = 0;
static thread_list sleeping_list = THREAD_LIST_INIT;

// Only to be called in scheduler context
void timing_uptime_tick_us(unsigned long long addend) {
    uptime_us += addend;
}

// Only to be called in scheduler context
void timing_uptime_wake_sleeping(void) {
    thread *this_thread = sleeping_list.head;
    thread **last_thread = &sleeping_list.head;
    sleeping_list.tail = NULL;
    while (this_thread) {
        thread *next_this_thread = this_thread->next;
        thread **next_last_thread = &this_thread->next;

        if (this_thread->sleep_until_us < uptime_us) {
            *last_thread = this_thread->next;
            this_thread->next = NULL;
            thread_unblock(this_thread);
        } else {
            sleeping_list.tail = this_thread;
            last_thread = next_last_thread;
        }

        this_thread = next_this_thread;
    }
}

unsigned long long timing_uptime_get_us(void) {
    return uptime_us;
}

void timing_sleep_until_us(unsigned long long time_us) {
    thread_current->sleep_until_us = time_us;
    thread_block_to_list(THREAD_STATE_SLEEPING, &sleeping_list);
}
