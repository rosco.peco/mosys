#include "semaphore.h"

void semaphore_init(semaphore *smphr, semaphore_value value) {
    atomic_init(&smphr->value, value);
}

void semaphore_wait(semaphore *smphr) {
    semaphore_value old_value = atomic_fetch_sub(&smphr->value, 1);
    if (old_value > 0) {
        return;
    }
    // TODO: Make sure nothing happens between these lines
    thread_block_to_list(THREAD_STATE_SEMAPHORE, &smphr->waiting_list);
}

void semaphore_signal(semaphore *smphr) {
    atomic_fetch_add(&smphr->value, 1);
    thread_unblock_from_list(&smphr->waiting_list);
}
