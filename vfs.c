#include "vfs.h"
#include "errno.h"
#include "kalloc.h"
#include "process.h"
#include "panic.h"
#include "rwlock.h"
#include <limits.h>
#include <string.h>
#include <stdint.h>

static filesystem_type *filesystem_type_list = NULL;
static rwlock filesystem_type_list_rwl = RWLOCK_INIT;

static mountpoint *mountpoint_list = NULL;
static rwlock mountpoint_list_rwl = RWLOCK_INIT;

void vfs_filesystem_type_register(filesystem_type *fst) {
    rwlock_write_begin(&filesystem_type_list_rwl);

    // Advance next_ptr to point to the current NULL pointer in list
    filesystem_type **next_ptr = &filesystem_type_list;
    while (*next_ptr != NULL) {
        next_ptr = &((*next_ptr)->next);
    }

    // Add fst to list
    fst->next = NULL;
    *next_ptr = fst;

    rwlock_write_end(&filesystem_type_list_rwl);
}

superblock *vfs_superblock_new(filesystem_type *type) {
    superblock *sb = kalloc(sizeof(superblock));

    if (sb) {
        memset(sb, 0, sizeof(*sb));
        mutex_init(&sb->mtx);
        sb->type = type;
    }

    return sb;
}

void vfs_superblock_delete(superblock *sb) {
    kfree(sb);
}

mountpoint *vfs_mountpoint_new(superblock *sb, const char *mount_path) {
    mountpoint *mp = kalloc(sizeof(mountpoint));

    if (!mp) {
        return NULL;
    }

    memset(mp, 0, sizeof(*mp));
    mp->ref_count = 1;
    mp->sb = sb;
    mp->mount_path = mount_path;

    rwlock_write_begin(&mountpoint_list_rwl);

    // Advance next_ptr to point to the current NULL pointer in list
    mountpoint **next_ptr = &mountpoint_list;
    while (*next_ptr != NULL) {
        next_ptr = &((*next_ptr)->next);
    }

    (*next_ptr) = mp;

    rwlock_write_end(&mountpoint_list_rwl);

    return mp;
}

// mountpoint_list_rwl must already be locked for write
void vfs_mountpoint_delete(mountpoint *mp) {
    // TODO: ACTUALLY REMOVE FROM LIST!!!

    if (mp->ref_count != 0) {
        panic("Tried to delete still-referenced mountpoint");
    }
    kfree(mp);
}

void vfs_mountpoint_ref_nolock(mountpoint *mp) {
    if (mp->ref_count == SIZE_MAX) {
        panic("Overflowed mountpoint ref_count");
    }
    mp->ref_count += 1;
}

void vfs_mountpoint_unref_nolock(mountpoint *mp) {
    if (mp->ref_count == 0) {
        panic("Underflowed mountpoint ref_count");
    }
    mp->ref_count -= 1;
    
    if (mp->ref_count == 0) {
        vfs_mountpoint_delete(mp);
        mp = NULL;
    }
}

void vfs_mountpoint_ref(mountpoint *mp) {
    rwlock_write_begin(&mountpoint_list_rwl);
    vfs_mountpoint_ref_nolock(mp);
    rwlock_write_end(&mountpoint_list_rwl);
}

void vfs_mountpoint_unref(mountpoint *mp) {
    rwlock_write_begin(&mountpoint_list_rwl);
    vfs_mountpoint_unref_nolock(mp);
    rwlock_write_end(&mountpoint_list_rwl);
}

mountpoint *vfs_mountpoint_lookup(const char *path, const char **subpath_ptr) {
    rwlock_read_begin(&mountpoint_list_rwl);

    size_t best_len = 0;
    mountpoint *best_mp = NULL;

    for (mountpoint *it = mountpoint_list; it; it = it->next) {
        size_t mount_path_len = strlen(it->mount_path);
        // TODO: This is a note that the broken strncmp works here because n < strlen([...]); remove this note when strnlen is fixed
        if (0 == strncmp(path, it->mount_path, mount_path_len)) {
            // Found matching
            if (mount_path_len > best_len) {
                // Is new longest
                best_len = mount_path_len;
                best_mp = it;
            }
        }
    }

    if (best_mp) {
        *subpath_ptr = path + best_len;
    }

    rwlock_read_end(&mountpoint_list_rwl);

    vfs_mountpoint_ref(best_mp);
    
    return best_mp;
}

vnode *vfs_vnode_new(superblock *sb) {
    vnode *vn = kalloc(sizeof(vnode));

    if (vn) {
        memset(vn, 0, sizeof(*vn));
        mutex_init(&vn->mtx);
        vn->ref_count = 1;
        vn->sb = sb;
    }

    return vn;
}

void vfs_vnode_delete(vnode *vn) {
    if (vn->ref_count != 0) {
        panic("Tried to delete still-referenced vnode");
    }
    kfree(vn);
}

void vfs_vnode_ref(vnode *vn) {
    mutex_lock(&vn->mtx);

    if (vn->ref_count == SIZE_MAX) {
        panic("Overflowed vnode ref_count");
    }
    vn->ref_count += 1;

    mutex_unlock(&vn->mtx);
}

void vfs_vnode_unref(vnode *vn) {
    mutex_lock(&vn->mtx);

    if (vn->ref_count == 0) {
        panic("Underflowed vnode ref_count");
    }
    vn->ref_count -= 1;
    
    if (vn->ref_count == 0) {
        vfs_vnode_delete(vn);
        vn = NULL;
    } else {
        mutex_unlock(&vn->mtx);
    }
}

// Calling function must call vfs_fdesk_unref on non-NULL return value when done
static inline fdesc *fdesc_from_fd(int fd) {
    if (fd < 0 || fd >= MAX_OPEN_FILES) {
        return NULL;
    }

    fdesc_list *fdl = thread_current->owner_process->fdl;
    if (!fdl) {
        panic("Process has no file descriptor list");
    }

    mutex_lock(&fdl->mtx);
    fdesc *fdsc = fdl->fds[fd];
    if (fdsc) {
        vfs_fdesc_ref(fdsc);
    }
    mutex_unlock(&fdl->mtx);
    
    return fdsc;
}

fdesc *vfs_fdesc_new(void) {
    fdesc *fdsc = kalloc(sizeof(fdesc));

    if (fdsc) {
        memset(fdsc, 0, sizeof(*fdsc));
        mutex_init(&fdsc->mtx);
        fdsc->ref_count = 1;
    }

    return fdsc;
}

void vfs_fdesc_delete(fdesc *fdsc) {
    if (fdsc->ref_count != 0) {
        panic("Tried to delete still-referenced fdesc");
    }
    kfree(fdsc);
}

void vfs_fdesc_ref(fdesc *fdsc) {
    mutex_lock(&fdsc->mtx);

    if (fdsc->ref_count == SIZE_MAX) {
        panic("Overflowed fdesc ref_count");
    }
    fdsc->ref_count += 1;

    mutex_unlock(&fdsc->mtx);
}

void vfs_fdesc_unref(fdesc *fdsc) {
    mutex_lock(&fdsc->mtx);

    if (fdsc->ref_count == 0) {
        panic("Underflowed fdesc ref_count");
    }
    fdsc->ref_count -= 1;

    if (fdsc->ref_count == 0) {
        vfs_fdesc_delete(fdsc);
        fdsc = NULL;     // fdsc is no longer valid
    } else {
        mutex_unlock(&fdsc->mtx);
    }
}

fdesc *vfs_fdesc_clone(fdesc *src_fdsc) {
    mutex_lock(&src_fdsc->mtx);
    fdesc *dest_fdsc = vfs_fdesc_new();

    if (dest_fdsc) {
        vfs_vnode_ref(src_fdsc->vn);
        dest_fdsc->vn = src_fdsc->vn;

        dest_fdsc->mode = src_fdsc->mode;
        dest_fdsc->flags = src_fdsc->flags;

        dest_fdsc->data_ptr = src_fdsc->data_ptr; // TODO: Add ability to clone data by function pointer

        dest_fdsc->offset = src_fdsc->offset;
    }

    mutex_unlock(&src_fdsc->mtx);
    return dest_fdsc;
}

fdesc_list *vfs_fdesc_list_new(void) {
    fdesc_list *fdl = kalloc(sizeof(fdesc_list));

    if (!fdl) {
        return NULL;
    }

    memset(fdl, 0, sizeof(*fdl));
    mutex_init(&fdl->mtx);
    fdl->ref_count = 1;

    return fdl;
}

// Mutex fdl->mtx may already be locked
void vfs_fdesc_list_delete(fdesc_list *fdl) {
    if (fdl->ref_count != 0) {
        panic("Tried to delete still-referenced fdesc_list");
    }

    kfree(fdl);
}

void vfs_fdesc_list_ref(fdesc_list *fdl) {
    mutex_lock(&fdl->mtx);

    if (fdl->ref_count == SIZE_MAX) {
        panic("Overflowed fdesc_list ref_count");
    }
    fdl->ref_count += 1;

    mutex_unlock(&fdl->mtx);
}

void vfs_fdesc_list_unref(fdesc_list *fdl) {
    mutex_lock(&fdl->mtx);

    if (fdl->ref_count == 0) {
        panic("Underflowed fdesc_list ref_count");
    }
    fdl->ref_count -= 1;
    
    if (fdl->ref_count == 0) {
        vfs_fdesc_list_delete(fdl);
        fdl = NULL;
    } else {
        mutex_unlock(&fdl->mtx);
    }
}

fdesc_list *vfs_fdesc_list_clone(fdesc_list *src_fdl) {
    fdesc_list *dest_fdl = vfs_fdesc_list_new();

    if (!dest_fdl) {
        return NULL;
    }

    mutex_lock(&src_fdl->mtx);

    for (size_t i = 0; i < MAX_OPEN_FILES; ++i) {
        if (src_fdl->fds[i]) {
            dest_fdl->fds[i] = vfs_fdesc_clone(src_fdl->fds[i]);
        }
    }

    mutex_unlock(&src_fdl->mtx);

    return dest_fdl;
}

int vfs_open(size_t pathname_len, const char pathname[pathname_len], int flags, mode_t mode, int *errno_ptr) {
    fdesc *fdsc = vfs_open_fdesc(pathname_len, pathname, flags, mode, errno_ptr);
    if (!fdsc) {
        return -1;
    }

    // Add file to process's file descriptor list
    fdesc_list *fdl = thread_current->owner_process->fdl;
    if (!fdl) {
        panic("Process has no file descriptor list");
    }
    mutex_lock(&fdl->mtx);
    int fd = -1;
    for (size_t i = 0; i < MAX_OPEN_FILES; ++i) {
        if (!fdl->fds[i]) {
            fdl->fds[i] = fdsc;
            fd = i;
            break;
        }
    }
    mutex_unlock(&fdl->mtx);

    return fd;
}

int vfs_close(int fd, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    int result = vfs_close_fdesc(fdsc, errno_ptr);

    // Remove file descriptor from process's file descriptor list
    fdesc_list *fdl = thread_current->owner_process->fdl;
    if (!fdl) {
        panic("Process has no file descriptor list");
    }
    mutex_lock(&fdl->mtx);
    fdl->fds[fd] = NULL;
    vfs_fdesc_unref(fdsc);
    mutex_unlock(&thread_current->owner_process->fdl->mtx);

    superblock *sb = fdsc->vn->sb;

    // Destroy the file descriptor
    if (!sb->type || !sb->type->fsops || !sb->type->fsops->destroy) {
        panic("Expected to find destroy for mount");
    }
    sb->type->fsops->destroy(sb, fdsc->vn);
    fdsc->vn = NULL;
    vfs_fdesc_unref(fdsc);
    fdsc = NULL;

    return result;
}

ssize_t vfs_read(int fd, void *buf, size_t count, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    ssize_t result = vfs_read_fdesc(fdsc, buf, count, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

ssize_t vfs_write(int fd, const void *buf, size_t count, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    ssize_t result = vfs_write_fdesc(fdsc, buf, count, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

int vfs_ioctl(int fd, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    int result = vfs_ioctl_fdesc(fdsc, request, argp, check_user_pointers, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

ssize_t vfs_getdents(int fd, dirent *drnt, size_t count, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    ssize_t result = vfs_getdents_fdesc(fdsc, drnt, count, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

// TODO: Should we lock vn before the fops calls?

fdesc *vfs_open_fdesc(size_t pathname_len, const char pathname[pathname_len], int flags, mode_t mode, int *errno_ptr) {
    // TODO: Handle O_CREATE and O_EXCL

    // Find mount point from path
    const char *subpath;
    mountpoint *found_mount = vfs_mountpoint_lookup(pathname, &subpath);
    if (!found_mount) {
        *errno_ptr = ENOENT;
        return NULL;
    }

    // Get superblock from found mount point
    superblock *sb = found_mount->sb;
    if (!sb) {
        panic("Found mount point has no superblock");
    }

    // Get lookup vnode from superblock
    if (!sb->type || !sb->type->fsops || !sb->type->fsops->lookup) {
        panic("Expected to find lookup for mount");
    }
    vnode *vn = sb->type->fsops->lookup(sb, subpath);
    if (!vn) {
        *errno_ptr = -ENOENT;
        goto failed_lookup;
    }

    fdesc *fdsc = vfs_fdesc_new();
    if (!fdsc) {
        *errno_ptr = ENOMEM;
        goto failed_fdesc_new;
    }

    mutex_lock(&fdsc->mtx);

    fdsc->vn = vn;

    fdsc->flags = flags;
    fdsc->mode = mode;
    fdsc->offset = 0;

    // TODO: Check actual file information in vnode from fsops->lookup()?

    // Call fops->open()
    if (vn->fops && vn->fops->open) {
        if (vn->fops->open(fdsc, errno_ptr) == -1) {
            goto failed_open;
        }
    }

    // TODO: Seek to end if O_APPEND

    mutex_unlock(&fdsc->mtx);
    return fdsc;

failed_open:
    mutex_unlock(&fdsc->mtx);
    vfs_fdesc_unref(fdsc);
    fdsc = NULL;
failed_fdesc_new:
    if (!sb->type || !sb->type->fsops || !sb->type->fsops->destroy) {
        panic("Expected to find destroy for mount");
    }
    sb->type->fsops->destroy(sb, vn);
    vn = NULL;
failed_lookup:
    return NULL;
}

int vfs_close_fdesc(fdesc *fdsc, int *errno_ptr) {
    int result;
    mutex_lock(&fdsc->mtx);

    if (!fdsc->vn) {
        panic("Expected vnode");
    }

    if (!fdsc->vn->fops) {
        panic("Expected file_operations");
    } else if (fdsc->vn->fops->close) {
        result = fdsc->vn->fops->close(fdsc, errno_ptr);
    } else {
        result = 0;
    }
    
    mutex_unlock(&fdsc->mtx);
    return result;
}

ssize_t vfs_read_fdesc(fdesc *fdsc, void *buf, size_t count, int *errno_ptr) {
    ssize_t result;
    mutex_lock(&fdsc->mtx);

    if (!fdsc->vn) {
        panic("Expected vnode");
    }

    if (!(fdsc->flags & O_RDONLY)) {
        *errno_ptr = EINVAL; 
        result = -1;
        goto done;
    }

    if (!fdsc->vn->fops) {
        panic("Expected file_operations");
    } else if (fdsc->vn->fops->read) {
        result = fdsc->vn->fops->read(fdsc, buf, count, errno_ptr);
    } else {
        *errno_ptr = EINVAL; 
        result = -1;
        goto done;
    }

    if (result == -1) {
        goto done;
    }

    fdsc->offset += result;

done:
    mutex_unlock(&fdsc->mtx);
    return result;
}

ssize_t vfs_write_fdesc(fdesc *fdsc, const void *buf, size_t count, int *errno_ptr) {
    ssize_t result;
    mutex_lock(&fdsc->mtx);

    if (!fdsc->vn) {
        panic("Expected vnode");
    }

    if (!(fdsc->flags & O_WRONLY)) {
        *errno_ptr = EINVAL; 
        result = -1;
        goto done;
    }

    if (!fdsc->vn->fops) {
        panic("Expected file_operations");
    } else if (fdsc->vn->fops->write) {
        result = fdsc->vn->fops->write(fdsc, buf, count, errno_ptr);
    } else {
        *errno_ptr = EINVAL; 
        result = -1;
        goto done;
    }

    if (result == -1) {
        goto done;
    }

    fdsc->offset += result;

done:
    mutex_unlock(&fdsc->mtx);
    return result;
}

int vfs_ioctl_fdesc(fdesc *fdsc, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr) {
    int result;
    mutex_lock(&fdsc->mtx);

    if (!fdsc->vn) {
        panic("Expected vnode");
    }

    if (fdsc->vn->type != FILE_TYPE_DEVICE) {
        *errno_ptr = ENOTTY; 
        result = -1;
        goto done;
    }

    if (!fdsc->vn->fops) {
        panic("Expected file_operations");
    } else if (fdsc->vn->fops->ioctl) {
        result = fdsc->vn->fops->ioctl(fdsc, request, argp, check_user_pointers, errno_ptr);
    } else {
        *errno_ptr = EINVAL; 
        result = -1;
        goto done;
    }

    if (result == -1) {
        goto done;
    }

done:
    mutex_unlock(&fdsc->mtx);
    return result;
}

ssize_t vfs_getdents_fdesc(fdesc *fdsc, dirent *drnt, size_t count, int *errno_ptr) {
    ssize_t result;
    mutex_lock(&fdsc->mtx);

    if (!fdsc->vn) {
        panic("Expected vnode");
    }

    if (fdsc->vn->type != FILE_TYPE_DIRECTORY) {
        *errno_ptr = ENOTDIR; 
        result = -1;
        goto done;
    }

    if (!fdsc->vn->fops) {
        panic("Expected file_operations");
    } else if (fdsc->vn->fops->getdents) {
        result = fdsc->vn->fops->getdents(fdsc, drnt, count, errno_ptr);
    } else {
        *errno_ptr = EINVAL; 
        result = -1;
        goto done;
    }

    if (result == -1) {
        goto done;
    }

done:
    mutex_unlock(&fdsc->mtx);
    return result;
}
