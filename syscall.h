#ifndef INCLUDE_SYSCALL_H
#define INCLUDE_SYSCALL_H

#include "kalloc.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct syscall_state {
    long d[8];
    long a[7];
    long usp;
    uint16_t sr;
    long pc;
    uint16_t format_vector_offset;
} __attribute__((packed));

void syscall_init(void);

// These two functions should not be called from C
// They don't expect a return address on the stack
extern void syscall_entry(void);
extern void syscall_leave(void);

void syscall(volatile struct syscall_state state);
void syscall_print_state(volatile const struct syscall_state *state);

bool object_is_in_range(const void *ptr, size_t size, ptr_range range);
bool user_ptr_is_valid(const void *ptr, size_t size) ;
bool user_strlen(const char *str, size_t *result);
bool user_string_vector_len(char *const *vect, size_t *result);

#endif
