#ifndef INCLUDE_CIRBUF_H
#define INCLUDE_CIRBUF_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct circbuf_uint8_t {
    uint8_t *buf;
    size_t size;
    size_t head;
    size_t tail;
} circbuf_uint8_t;

bool circbuf_uint8_t_is_empty(const circbuf_uint8_t *crcbf);
bool circbuf_uint8_t_is_full(const circbuf_uint8_t *crcbf);
bool circbuf_uint8_t_put(circbuf_uint8_t *crcbf, uint8_t value);
bool circbuf_uint8_t_get(circbuf_uint8_t *crcbf, uint8_t *value_p);

#endif
