#include "thread.h"
#include "process.h"
#include "kalloc.h"
#include "mfp68901.h"
#include "panic.h"
#include "ownedmem.h"
#include "processor.h"
#include "timing.h"
#include <machine.h>
#include <stdbool.h>
#include <string.h>

thread *thread_idle = NULL;
thread *thread_current = NULL;

thread_list thread_ready_list = THREAD_LIST_INIT;
thread_list thread_terminated_list = THREAD_LIST_INIT;

size_t irq_disable_counter = 0;

size_t thread_switch_postpone_counter = 0;
bool thread_switch_postponed_flag = false;

void thread_system_tick(void);

void thread_init(void) {
    thread_idle = thread_new();
    if (!thread_idle) {
        panic("Failed to create initial thread");
    }

    thread_current = thread_idle;
    thread_current->state = THREAD_STATE_RUNNING;

    mfp68901_interrupt_install(thread_system_tick, MFP68901_INTERRUPT_TIMER_C);
}

thread *thread_new(void) {
    thread *new_thread = kalloc(sizeof(thread));
    if (new_thread) {
        memset(new_thread, 0, sizeof(*new_thread));
    }
    return new_thread;
}

void thread_delete(thread *thread_ptr) {
    if (thread_ptr == thread_current) {
        panic("Tried to delete current thread");
    }
    kfree(thread_ptr);
}

__attribute__((interrupt)) void thread_system_tick(void) {
    thread_scheduler_lock();
    timing_uptime_tick_us(4991);    // TODO: Don't hardcode a magic number
    mfp68901_isr_bitclear(MFP68901_INTERRUPT_TIMER_C);
    timing_uptime_wake_sleeping();
    thread_schedule();
    thread_scheduler_unlock();
}

void thread_setup(thread *thread_ptr, void (*func)(), ownedmem *sup_stack, ownedmem *usr_stack) {
    thread_ptr->sup_stack = sup_stack;
    thread_ptr->usr_stack = usr_stack;
    thread_ptr->ssp = sup_stack->range.end;

    thread_setup_stack(thread_ptr, func);
}

void thread_setup_clone(thread *thread_ptr) {
    thread_ptr->sup_stack = ownedmem_clone_starting_at(thread_current->sup_stack, (void *) thread_current->user_state);
    thread_ptr->owner_process->user_space = ownedmem_ref(thread_current->owner_process->user_space);
    thread_ptr->owner_process->arg_env_space = ownedmem_ref(thread_current->owner_process->arg_env_space);
    thread_ptr->usr_stack = ownedmem_ref(thread_current->usr_stack);

    thread_ptr->user_state = (struct syscall_state *) (
        (uintptr_t) thread_current->user_state -
        (uintptr_t) thread_current->sup_stack->range.start +
        (uintptr_t) thread_ptr->sup_stack->range.start);

    // Child returns with 0
    // TODO: Does it always?
    thread_ptr->user_state->d[0] = 0;

    // Setup stack for thread_switch()
    thread_ptr->ssp = (void *) thread_ptr->user_state;
    thread_setup_stack(thread_ptr, syscall_leave);
}

noreturn void thread_idle_loop(void) {
    while (1) {
        thread_scheduler_lock();
        thread_schedule();
        thread_scheduler_unlock();
    }
}

inline void thread_scheduler_lock(void) {
    // TODO: SMP requires spinlock
    asm ("ori #(7 << 8), %SR");
    irq_disable_counter++;
}

inline void thread_scheduler_unlock(void) {
    // TODO: SMP requires spinlock
    if (--irq_disable_counter == 0) {
        asm ("andi #~(7 << 8), %SR");
    }
}

void thread_differ_switches(void) {
    asm ("ori #(7 << 8), %SR");
    irq_disable_counter++;
    thread_switch_postpone_counter++;
}

void thread_undiffer_switches(void) {
    if (--thread_switch_postpone_counter == 0) {
        if (thread_switch_postponed_flag) {
            thread_switch_postponed_flag = false;
            thread_schedule();
        }
    }
    if (--irq_disable_counter == 0) {
        asm ("andi #~(7 << 8), %SR");
    }
}

void thread_block(thread_state state) {
    thread_scheduler_lock();
    thread_current->state = state;
    thread_schedule();
    thread_scheduler_unlock();
}

bool thread_block_while_unblocking_from_list(thread_state state, thread_list *unblock_from_list) {
    thread_scheduler_lock();
    bool did_unblock = thread_unblock_from_list(unblock_from_list);
    thread_current->state = state;
    thread_schedule();
    thread_scheduler_unlock();
    return did_unblock;
}

size_t thread_block_while_unblocking_all_from_list(thread_state state, thread_list *unblock_from_list) {
    thread_scheduler_lock();
    size_t num_unblocked = thread_unblock_all_from_list(unblock_from_list);
    thread_current->state = state;
    thread_schedule();
    thread_scheduler_unlock();
    return num_unblocked;
}

void thread_block_to_list(thread_state state, thread_list *block_to_list) {
    thread_scheduler_lock();
    thread_current->state = state;
    thread_list_enqueue(block_to_list, thread_current);
    thread_schedule();
    thread_scheduler_unlock();
}

bool thread_block_to_list_while_unblocking_from_list(thread_state state, thread_list *block_to_list, thread_list *unblock_from_list) {
    thread_scheduler_lock();
    bool did_unblock = thread_unblock_from_list(unblock_from_list);
    thread_current->state = state;
    thread_list_enqueue(block_to_list, thread_current);
    thread_schedule();
    thread_scheduler_unlock();
    return did_unblock;
}

size_t thread_block_to_list_while_unblocking_all_from_list(thread_state state, thread_list *block_to_list, thread_list *unblock_from_list) {
    thread_scheduler_lock();
    size_t num_unblocked = thread_unblock_all_from_list(unblock_from_list);
    thread_current->state = state;
    thread_list_enqueue(block_to_list, thread_current);
    thread_schedule();
    thread_scheduler_unlock();
    return num_unblocked;
}

void thread_unblock(thread *unblocked_thread) {
    thread_scheduler_lock();
    unblocked_thread->state = THREAD_STATE_READY;
    thread_list_enqueue(&thread_ready_list, unblocked_thread);
    thread_scheduler_unlock();
}

bool thread_unblock_from_list(thread_list *unblock_from_list) {
    thread_scheduler_lock();
    thread *unblocked_thread = thread_list_dequeue(unblock_from_list);
    if (unblocked_thread) {
        thread_unblock(unblocked_thread);
    }
    thread_scheduler_unlock();
    return unblocked_thread != NULL;
}

size_t thread_unblock_all_from_list(thread_list *unblock_from_list) {
    thread_scheduler_lock();
    size_t num_unblocked;
    for (num_unblocked = 0; thread_unblock_from_list(unblock_from_list); ++num_unblocked) {
        // Loop to unblock all
    }
    thread_scheduler_unlock();
    return num_unblocked;
}

// This must be called with scheduler locked
void thread_schedule(void) {
    if (thread_switch_postpone_counter > 0) {
        thread_switch_postponed_flag = true;
        return;
    }

    thread *next_thread = thread_list_dequeue(&thread_ready_list);

    if (next_thread) {
        if (next_thread == thread_idle) {
            // Next thread is the idle thread, try to find an alternative
            thread *non_idle_next_thread = thread_list_dequeue(&thread_ready_list);
            if (non_idle_next_thread) {
                // Found another thread to run, re-enqueue idle thread and switch to other
                thread_list_enqueue(&thread_ready_list, next_thread);
                thread_switch(non_idle_next_thread);
                return;
            } else if (thread_current->state == THREAD_STATE_RUNNING) {
                // This thread can keep running
                thread_list_enqueue(&thread_ready_list, next_thread);
                return;
            }
        }
        // Switch to first thread dequeued
        thread_switch(next_thread);
    } else if (thread_current->state == THREAD_STATE_RUNNING) {
        return;
    } else {
        panic("No thread available to run");
    }
}

void thread_terminate(void) {
    thread_differ_switches();

    // Clean thread resources here

    thread_block_to_list(THREAD_STATE_TERMINATED, &thread_terminated_list);

    // TODO: Make sure we can get properly cleaned up

    thread_undiffer_switches();

    panic("Terminated thread continued");
}

// Only to be used by thread_switch
void thread_on_start(void) {
    thread_scheduler_unlock();
}

// Only to be used by thread_switch
void thread_pre_switch(void) {
    // Make current thread be ready if it's running
    if (thread_current->state == THREAD_STATE_RUNNING) {
        thread_current->state = THREAD_STATE_READY;
        thread_list_enqueue(&thread_ready_list, thread_current);
    }
}

// Only to be used by thread_switch
void thread_post_switch(void) {
    // Make current thread be running
    thread_current->state = THREAD_STATE_RUNNING;
}

void thread_list_enqueue(thread_list *list, thread *enq_thread) {
    enq_thread->next = NULL;
    spinlock_lock(&list->lock);

    if (list->tail) {
        list->tail->next = enq_thread;
    } else {
        list->head = enq_thread;
    }

    list->tail = enq_thread;
    spinlock_unlock(&list->lock);
}

thread *thread_list_dequeue(thread_list *list) {
    spinlock_lock(&list->lock);
    thread *deq_thread = list->head;

    if (deq_thread) {
        list->head = deq_thread->next;
        deq_thread->next = NULL;

        if (!list->head) {
            list->tail = NULL;
        }
    }

    spinlock_unlock(&list->lock);
    return deq_thread;
}
