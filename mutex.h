#ifndef INCLUDE_MUTEX_H
#define INCLUDE_MUTEX_H

#include "thread.h"
#include <stdatomic.h>
#include <stdbool.h>

typedef struct mutex {
    atomic_flag flag;
    thread_list waiting_list;
} mutex;

#define MUTEX_INIT { ATOMIC_FLAG_INIT, THREAD_LIST_INIT }

void mutex_init(mutex *mtx);
void mutex_lock(mutex *mtx);
bool mutex_trylock(mutex *mtx);
void mutex_unlock(mutex *mtx);

#endif
