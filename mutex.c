#include "mutex.h"
#include <string.h>

void mutex_init(mutex *mtx) {
    const static mutex tmp =  MUTEX_INIT;
    memcpy(mtx , &tmp, sizeof(*mtx));
}

void mutex_lock(mutex *mtx) {
    bool already_locked = atomic_flag_test_and_set(&mtx->flag);
    if (!already_locked) {
        return;
    }
    // TODO: Make sure nothing happens between these lines
    thread_block_to_list(THREAD_STATE_MUTEX, &mtx->waiting_list);
}

bool mutex_trylock(mutex *mtx) {
    bool already_locked = atomic_flag_test_and_set(&mtx->flag);
    return !already_locked;
}

void mutex_unlock(mutex *mtx) {
    if (!thread_unblock_from_list(&mtx->waiting_list)) {
        atomic_flag_clear(&mtx->flag);
    }
}
