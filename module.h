#ifndef INCLUDE_MODULE_H
#define INCLUDE_MODULE_H

#include <stdint.h>

typedef int (*module_init)(void);
typedef void (*module_deinit)(void);

typedef struct module {
    module_init init;
    module_deinit deinit;
    const char *name;
} module;

#define MODULE_DEFINE(module_desc) static module * const module_desc ## _ptr __attribute__((used, section ("module_tab"))) = &module_desc

int module_init_static();
void module_deinit_static();

#endif
