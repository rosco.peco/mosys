#ifndef INCLUDE_SPINLOCK_H
#define INCLUDE_SPINLOCK_H

#include <stdatomic.h>

typedef struct {
    atomic_flag flag;
} spinlock;

#define SPINLOCK_INIT { ATOMIC_FLAG_INIT }

static inline void spinlock_lock(spinlock *spnlck) {
    while (atomic_flag_test_and_set(&spnlck->flag)) {
        // Spin
    }
}

static inline void spinlock_unlock(spinlock *spnlck) {
    atomic_flag_clear(&spnlck->flag);
}

#endif
