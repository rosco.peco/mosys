#include "kalloc.h"
#include "mutex.h"
#include "panic.h"
#include <stdalign.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#define KHEAP_SIZE 0x80000

extern uint32_t _init_end;

static char *const kheap = (char *) &_init_end + 0x10000;
static char *kheap_pointer = (char *) &_init_end + 0x10000;
static mutex kheap_mutex = MUTEX_INIT;

ptr_range kalloc_range(size_t size) {
    mutex_lock(&kheap_mutex);

    // Realign kheap_pointer for any scalar type
    kheap_pointer += alignof(max_align_t) - 1;
    kheap_pointer = (char *) ((uintptr_t) kheap_pointer / alignof(max_align_t) * alignof(max_align_t));

    void *start = kheap_pointer;
    kheap_pointer += size;

    void *end = kheap_pointer;

    if (kheap_pointer >= kheap + KHEAP_SIZE) {
        panic("Out of dynamic memory");
    }

    mutex_unlock(&kheap_mutex);

    return (ptr_range) { start, end };
}

void *kalloc(size_t size) {
    return kalloc_range(size).start;
}

void *kalloc_end(size_t size) {
    return kalloc_range(size).end;
}

ptr_range kalloc_clone(ptr_range orig) {
    size_t size = (char *) orig.end - (char *) orig.start;
    ptr_range result = kalloc_range(size);

    if (result.start) {
        memcpy(result.start, orig.start, size);
    }

    return result;
}

ptr_range kalloc_clone_starting_at(ptr_range orig, void *starting_at) {
    if (starting_at < orig.start || starting_at > orig.end) {
        return PTR_RANGE_NULL;
    }

    size_t size = (char *) orig.end - (char *) orig.start;
    size_t offset_copied = (char *) starting_at - (char *) orig.start;
    size_t size_copied = size - offset_copied;
    ptr_range result = kalloc_range(size);

    if (result.start) {
        memcpy((char *) result.start + offset_copied, starting_at, size_copied);
    }

    return result;
}

ptr_range kalloc_clone_ending_at(ptr_range orig, void *ending_at) {
    if (ending_at < orig.start || ending_at > orig.end) {
        return PTR_RANGE_NULL;
    }

    size_t size = (char *) orig.end - (char *) orig.start;
    size_t size_copied = (char *) ending_at - (char *) orig.start;
    ptr_range result = kalloc_range(size);

    if (result.start) {
        memcpy(result.start, orig.start, size_copied);
    }

    return result;
}

void kfree(void *ptr) {
    (void) ptr;
}
