#ifndef INCLUDE_SEMAPHORE_H
#define INCLUDE_SEMAPHORE_H

#include "thread.h"
#include <stdatomic.h>
#include <stdbool.h>

typedef int semaphore_value;

typedef struct semaphore {
    _Atomic semaphore_value value;
    thread_list waiting_list;
} semaphore;

void semaphore_init(semaphore *smphrm, semaphore_value value);
void semaphore_wait(semaphore *smphr);
void semaphore_signal(semaphore *smphr);

#endif
