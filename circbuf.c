#include "circbuf.h"

// Passing a size of 0 is invalid
static inline size_t next(size_t value, size_t size) {
    value += 1;
    if (value >= size)
        value = 0;
    return value;
}

#define next_head(crcbf) (next((crcbf)->head, (crcbf)->size))
#define next_tail(crcbf) (next((crcbf)->tail, (crcbf)->size))

inline bool circbuf_uint8_t_is_empty(const circbuf_uint8_t *crcbf) {
    return crcbf->tail == crcbf->head;
}

inline bool circbuf_uint8_t_is_full(const circbuf_uint8_t *crcbf) {
    return next_tail(crcbf) == crcbf->head;
}

bool circbuf_uint8_t_put(circbuf_uint8_t *crcbf, uint8_t value) {
    if (!circbuf_uint8_t_is_full(crcbf)) {
        crcbf->buf[crcbf->tail] = value;
        crcbf->tail = next_tail(crcbf);
        return true;
    } else {
        return false;
    }
}

bool circbuf_uint8_t_get(circbuf_uint8_t *crcbf, uint8_t *value_p) {
    if (!circbuf_uint8_t_is_empty(crcbf)) {
        *value_p = crcbf->buf[crcbf->head];
        crcbf->head = next_head(crcbf);
        return true;
    } else {
        return false;
    }
}
