#include "process.h"
#include "elf.h"
#include "errno.h"
#include "kalloc.h"
#include "mutex.h"
#include "panic.h"
#include "processor.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define CLONE_USER_STACK_BYTES_SAVED 64
// TODO: Don't hardcode EXEC_MAX_SIZE
#define EXEC_MAX_SIZE 0x8000
#define USR_STACK_SIZE 0x400

#define WCONTINUED  0x2
#define WNOHANG     0x1
#define WUNTRACED   0x4

static pid_t pid_next = 1;
mutex pid_next_mtx = MUTEX_INIT;

static ownedmem *process_make_arg_env_space(size_t argv_len, char *const argv[argv_len], char *const (**new_argv_ptr)[(argv_len == 0) ? 1 : argv_len],
                                            size_t envp_len, char *const envp[envp_len], char *const (**new_envp_ptr)[(envp_len == 0) ? 1 : envp_len]);

process *process_new(void) {
    thread *new_thread = thread_new();
    if (!new_thread) {
        return NULL;
    }

    process *new_process = kalloc(sizeof(process));
    if (!new_process) {
        thread_delete(new_thread);
        return NULL;
    }

    memset(new_process, 0, sizeof(*new_process));

    new_process->owned_thread = new_thread;
    new_thread->owner_process = new_process;

    mutex_lock(&pid_next_mtx);
    new_process->pid = pid_next++;
    mutex_unlock(&pid_next_mtx);
    if (thread_current && thread_current->owner_process) {
        new_process->parent = thread_current->owner_process;
        process_list_enqueue(&thread_current->owner_process->children, new_process);
    }

    return new_process;
}

void process_delete(process *process_ptr) {
    thread_delete(process_ptr->owned_thread);
    kfree(process_ptr);
}

void process_list_enqueue(process_list *list, process *enq_process) {
    enq_process->next = NULL;
    spinlock_lock(&list->lock);

    if (list->tail) {
        list->tail->next = enq_process;
    } else {
        list->head = enq_process;
    }

    list->tail = enq_process;
    spinlock_unlock(&list->lock);
}

process *process_list_dequeue(process_list *list) {
    spinlock_lock(&list->lock);
    process *deq_process = list->head;

    if (deq_process) {
        list->head = deq_process->next;
        deq_process->next = NULL;

        if (!list->head) {
            list->tail = NULL;
        }
    }

    spinlock_unlock(&list->lock);
    return deq_process;
}

// List must already be locked
bool process_list_remove(process_list *list, process *enq_process) {
    bool found = false;
    process **last_ptr = &list->head;
    process *last = NULL;
    process *current = list->head;

    // TODO: This is not well-written
    while (current) {
        if (current == enq_process) {
            found = true;
            *last_ptr = current->next;
            if (current == list->tail) {
                list->tail = last;
            }
            break;
        }

        last = current;
        current = current->next;
    }

    return found;
}

void process_setup(process *process_ptr, void (*func)(), ownedmem *sup_stack, ownedmem *usr_stack) {
    thread_setup(process_ptr->owned_thread, func, sup_stack, usr_stack);
}

noreturn void process_enter_sup(void *ssp, void *usp, process_entry pc, uint16_t sr) {
    process_enter(ssp, usp, pc, sr | 0x2000);
}

noreturn void process_enter_usr(void *ssp, void *usp, process_entry pc, uint16_t sr) {
    process_enter(ssp, usp, pc, sr & ~0x2000);
}

noreturn void process_enter(void *ssp, void *usp, process_entry pc, uint16_t sr) {
    struct syscall_state *state = process_enter_setup(ssp, usp, pc, sr);
    process_enter_state(state);
}

struct syscall_state *process_enter_setup(void *ssp, void *usp, process_entry pc, uint16_t sr) {
    struct syscall_state *state = (struct syscall_state *) ((char *) thread_current->sup_stack->range.end - sizeof(struct syscall_state));
    // TODO: Consider clearing other registers to avoid leaking state
    state->usp = (long) usp;
    state->sr = sr;
    state->pc = (long) pc;
    state->format_vector_offset = 0;
    return state;
}

process *process_clone(unsigned long flags, void *stack,
                    //    int *parent_tid, int *child_tid,
                    //    unsigned long tls,
                       int *errno_ptr) {

    // No MMU exists, so CLONE_VM is required
    // TODO: Decide if this is actually right
    if (!(flags & CLONE_VM)) {
        *errno_ptr = EINVAL;
        return NULL;
    }

    // TODO: Don't always require CLONE_VFORK
    if (!(flags & CLONE_VFORK)) {
        *errno_ptr = EINVAL;
        return NULL;
    }

    process *new_process = process_new();
    if (!new_process) {
        *errno_ptr = ENOMEM;
        return NULL;
    }

    new_process->fdl = vfs_fdesc_list_clone(thread_current->owner_process->fdl);
    if (!new_process->fdl) {
        process_delete(new_process);
        *errno_ptr = ENOMEM;
        return NULL;
    }

    thread_setup_clone(new_process->owned_thread);

    if (stack) {
        new_process->owned_thread->user_state->usp = (long) stack;
    }

    // Save some bytes above USP
    char saved_user_stack[CLONE_USER_STACK_BYTES_SAVED];
    memcpy(saved_user_stack, (void *) thread_current->user_state->usp, CLONE_USER_STACK_BYTES_SAVED);

    thread_unblock(new_process->owned_thread);

    if (flags & CLONE_VFORK) {
        thread_block_to_list(THREAD_STATE_VFORK, &new_process->owned_thread->vfork_thread_list);
    }

    // Restore saved bytes above USP
    memcpy((void *) thread_current->user_state->usp, saved_user_stack, CLONE_USER_STACK_BYTES_SAVED);

    return new_process;
}

static ownedmem *process_make_arg_env_space(size_t argv_len, char *const argv[argv_len], char *const (**new_argv_ptr)[(argv_len == 0) ? 1 : argv_len],
                                            size_t envp_len, char *const envp[envp_len], char *const (**new_envp_ptr)[(envp_len == 0) ? 1 : envp_len]) {
    // Input values are fully trusted

    // Create adjusted argv_len/envp_len for special cases where argv_len/envp_len are 0
    size_t new_argv_len = argv_len == 0 ? 1 : argv_len;
    size_t new_envp_len = envp_len == 0 ? 1 : envp_len;
                                                
    size_t argv_size = new_argv_len * sizeof(*argv);
    size_t envp_size = new_envp_len * sizeof(*envp);

    // argv/envp can only be NULL if argv_len/envp_len is 0
    // Only derefence argv/envp if argv_len/envp_len is non-0

    size_t argv_strings_size = 0;
    for (size_t i = 0; i < argv_len; ++i) {
        if (argv[i]) {
            argv_strings_size += strlen(argv[i]) + 1;   // Add one for NUL terminator
        }
    }
    size_t envp_strings_size = 0;
    for (size_t i = 0; i < envp_len; ++i) {
        if (envp[i]) {
            envp_strings_size += strlen(envp[i]) + 1;   // Add one for NUL terminator
        }
    }

    ownedmem *result = ownedmem_new(argv_size + envp_size + argv_strings_size + envp_strings_size);
    if (!result) {
        return NULL;
    }
    char *(*new_argv)[new_argv_len] = (char *(*)[new_argv_len]) ((char *) result->range.start);
    char *(*new_envp)[new_envp_len] = (char *(*)[new_envp_len]) ((char *) result->range.start + argv_size);
    char *new_argv_strings = (char *) result->range.start + argv_size + envp_size;
    char *new_envp_strings = (char *) result->range.start + argv_size + envp_size + argv_strings_size;
    char *new_arg_env_end = (char *) result->range.start + argv_size + envp_size + argv_strings_size + envp_strings_size;
    if (new_arg_env_end != result->range.end) {
        panic("Something went wrong in the arg_env size math");
    }

    char *argv_strings_position = new_argv_strings;
    for (size_t i = 0; i < new_argv_len; ++i) {
        if (i < argv_len && argv[i]) {
            (*new_argv)[i] = strcpy(argv_strings_position, argv[i]);
            argv_strings_position += strlen(argv_strings_position) + 1; // Add one for NUL terminator
        } else {
            (*new_argv)[i] = NULL;
        }
    }
    if (argv_strings_position != new_envp_strings) {
        panic("Something went wrong in the arg_env size math");
    }
    char *envp_strings_position = new_envp_strings;
    for (size_t i = 0; i < new_envp_len; ++i) {
        if (i < envp_len &&envp[i]) {
            (*new_envp)[i] = strcpy(envp_strings_position, envp[i]);
            envp_strings_position += strlen(envp_strings_position) + 1; // Add one for NUL terminator
        } else {
            (*new_envp)[i] = NULL;
        }
    }
    if (envp_strings_position != new_arg_env_end) {
        panic("Something went wrong in the arg_env size math");
    }

    *new_argv_ptr = (char *const (*)[new_argv_len]) new_argv;
    *new_envp_ptr = (char *const (*)[new_envp_len]) new_envp;
    return result;
}

int process_execve(size_t pathname_len, const char pathname[pathname_len],
                   size_t argv_len, char *const argv[argv_len],
                   size_t envp_len, char *const envp[envp_len],
                   int *errno_ptr) {
    // Declared here because it's before all gotos, otherwise compiler errors are generated
    char *const (*new_argv)[argv_len];
    char *const (*new_envp)[argv_len];

    char *exec_buf = kalloc(EXEC_MAX_SIZE);
    if (!exec_buf) {
        *errno_ptr = ENOMEM;
        goto failed_kalloc;
    }

    int exec_fd = vfs_open(pathname_len, pathname, O_RDONLY, 0, errno_ptr);
    if (exec_fd < 0) {
        // vfs_open() sets errno at errno_ptr
        goto failed_open;
    }

    ssize_t read_size = vfs_read(exec_fd, exec_buf, EXEC_MAX_SIZE, errno_ptr);
    if (read_size < 0) {
        // vfs_read() sets errno at errno_ptr
        goto failed_read;
    }

    int close_status = vfs_close(exec_fd, errno_ptr);
    if (close_status < 0) {
        panic("Failed to close file descriptor in execve");
        // vfs_close() sets errno at errno_ptr
        goto failed_close;
    }

    ownedmem *new_usr_stack = ownedmem_new(USR_STACK_SIZE);
    if (!new_usr_stack) {
        *errno_ptr = ENOMEM;
        goto failed_new_usr_stack;
    }

    // TODO: Add #! interpreter

    void *exec_entry = NULL;
    ownedmem *new_user_space = elf_load_file(exec_buf, &exec_entry);

    kfree(exec_buf);

    // TODO: When rewriting ELF code, provide a specific failure reason
    if (!new_user_space) {
        *errno_ptr = ENOMEM;
        goto failed_elf_load_file;
    } else if (!exec_entry) {
        *errno_ptr = ENOEXEC;
        goto failed_elf_load_file;
    }

    // Copy argv and envp to ownedmem at thread_current->owner_process->arg_env_space
    ownedmem *new_arg_env_space = process_make_arg_env_space(argv_len, argv, &new_argv, envp_len, envp, &new_envp);
    if (!new_arg_env_space) {
        *errno_ptr = ENOMEM;
        goto failed_new_arg_env_space;
    }

    // Use the new user space, not the parent's
    if (thread_current->owner_process->user_space) {
        ownedmem_unref(thread_current->owner_process->user_space);
    }
    thread_current->owner_process->user_space = new_user_space;

    // Use a new stack, not the parent's
    if (thread_current->usr_stack) {
        ownedmem_unref(thread_current->usr_stack);
    }
    thread_current->usr_stack = new_usr_stack;

    // Use the new arg_env space, not the parent's
    if (thread_current->owner_process->arg_env_space) {
        ownedmem_unref(thread_current->owner_process->arg_env_space);
    }
    thread_current->owner_process->arg_env_space = new_arg_env_space;

    // Unblock waiting parent
    thread_unblock_all_from_list(&thread_current->vfork_thread_list);

    if (thread_current->user_state) {
        memset((void *) thread_current->user_state->d, 0, sizeof(thread_current->user_state->d));
        memset((void *) thread_current->user_state->a, 0, sizeof(thread_current->user_state->a));
        thread_current->user_state->a[0] = (uintptr_t) new_argv;    // argv
        thread_current->user_state->a[1] = (uintptr_t) new_envp;    // envp
        thread_current->user_state->usp = (long) thread_current->usr_stack->range.end;
        thread_current->user_state->sr = 0x0000U;
        thread_current->user_state->pc = (long) exec_entry;
        thread_current->user_state->format_vector_offset = 0;
        return (argv_len == 0) ? 0 : argv_len - 1;                  // argc
    } else {
        // Note that argv and envp are only valid when thread_current->user_state is non-NULL
        process_enter_usr(thread_current->sup_stack->range.end, thread_current->usr_stack->range.end, (process_entry) (uintptr_t) exec_entry, 0x0000U);
        panic("process_enter_usr() returned");
    }

    int errno_dummy;    // Used to call cleanup functions without modifying actual failure cause
failed_new_arg_env_space:
failed_elf_load_file:
    goto failed_kalloc;
failed_new_usr_stack:
failed_close:
    goto failed_open;
failed_read:
    vfs_close(exec_fd, &errno_dummy);
failed_open:
    kfree(exec_buf);
failed_kalloc:
    return -1;
}

noreturn void process_exit_thread(int status) {
    thread_current->status = status & 0377;
    
    // TODO: Release all resources, this is a major memory leak

    // TODO: Transfer now-orphaned processes to PID 1

    // Unblock any vfork-waiting parent
    thread_unblock_all_from_list(&thread_current->vfork_thread_list);

    // TODO: Should this call thread_terminate?

    // TODO: If multi-threaded processes are added, we need to SIGEXIT them too
    // Unblock any waitpid-waiting parent
    thread_block_while_unblocking_all_from_list(THREAD_STATE_EXIT, &thread_current->owner_process->parent->owned_thread->waitpid_thread_list);

    // Disable interrupts and stop if PID 1 exits
    if (thread_current->owner_process->pid == 1) {
        while (1) {
            asm ("stop #(7 << 8)");
        }
    }

    panic("Thread unblocked from exit");
}

pid_t process_waitpid(pid_t pid, int *wstatus, int options, int *errno_ptr) {
    // Validate options
    if (options & ~(WNOHANG | WCONTINUED | WUNTRACED)) {
        *errno_ptr = EINVAL;
        return -1;
    }

    // TODO: Groups don't exist, related options are invalid
    if (pid == 0 || pid < -1) {
        *errno_ptr = EINVAL;
        return -1;
    }

    // TODO: These options aren't yet handled
    if (options & (WCONTINUED | WUNTRACED)) {
        *errno_ptr = EINVAL;
        return -1;
    }

    process *found_process = NULL;

    do {
        bool found_some_appropriate_pid = false;

        // TODO: Actually think through the locking here
        spinlock_lock(&thread_current->owner_process->children.lock);

        if (!thread_current->owner_process->children.head) {
            spinlock_unlock(&thread_current->owner_process->children.lock);
            *errno_ptr = ECHILD;
            return -1;
        }

        for (process *this_process = thread_current->owner_process->children.head;
             this_process;
             this_process = this_process->next) {
            if (pid != -1 && pid != this_process->pid) {
                // If we're looking for a specific PID, and this isn't it, continue
                continue;
            }

            found_some_appropriate_pid = true;

            if (this_process->owned_thread->state == THREAD_STATE_EXIT) {
                found_process = this_process;
                process_list_remove(&thread_current->owner_process->children, this_process);
                break;
            }
        }

        spinlock_unlock(&thread_current->owner_process->children.lock);

        if (found_process) {
            break;
        }

        if (!found_some_appropriate_pid) {
            *errno_ptr = ECHILD;
            return -1;
        }

        // Block and wait to be woken up by a child doing something relevant
        thread_block_to_list(THREAD_STATE_WAITPID, &thread_current->waitpid_thread_list);
    } while (!(options & WNOHANG));

    if (found_process) {
        if (wstatus) {
            *wstatus = found_process->owned_thread->status;
        }
        return found_process->pid;
    }

    return 0;
}
