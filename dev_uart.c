#include "circbuf.h"
#include "device.h"
#include "devreg.h"
#include "errno.h"
#include "mfp68901.h"
#include "module.h"
#include "panic.h"
#include "string.h"

static int dev_uart_init(void);
static void dev_uart_deinit(void);

static void uart_rcv_buf_full(void);
static void uart_rcv_err(void);
static void uart_xmit_buf_empty(void);
static void uart_xmit_err(void);

static ssize_t dev_uart_read(int minor, void *buf, size_t count, unsigned long offset, int *errno_ptr);
static ssize_t dev_uart_write(int minor, const void *buf, size_t count, unsigned long offset, int *errno_ptr);
static int dev_uart_ioctl(int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr);

static module dev_uart = {
    .init = dev_uart_init,
    .deinit = dev_uart_deinit,
    .name = "dev_uart",
};
MODULE_DEFINE(dev_uart);

static const int dev_uart_major = 2;

static uint8_t rx_buf[256];
circbuf_uint8_t rx_circbuf = {
    rx_buf,
    256,
};
static uint8_t tx_buf[256];
circbuf_uint8_t tx_circbuf = {
    tx_buf,
    256,
};

static device_operations dev_uart_dops = {
    dev_uart_read,
    dev_uart_write,
    dev_uart_ioctl,
};

static int dev_uart_init(void) {
    // Setup Timer D to 307.2 kHz = 3.6864 MHz / 4 / 3
    mfp68901_tcdcr_dc_set(MFP68901_TCDCR_DC_STOPPED);
    mfp68901_tddr_set(3);
    mfp68901_tcdcr_dc_set(MFP68901_TCDCR_DC_DELAY_4);

    // Set USART to /16 ASYNC 8N1
    mfp68901_ucr_set(MFP68901_UCR_CLK_16 | MFP68901_UCR_WL_8 | MFP68901_UCR_ST_1_1__ASYNC);

    // Set USART to enable RX
    mfp68901_rsr_set(MFP68901_RSR_RE);

    // Set USART idle TX pin state high and enable TX 
    mfp68901_tsr_set(MFP68901_TSR_HL_HIGH | MFP68901_TSR_TE);

    // Install interrupt handlers
    mfp68901_interrupt_install(uart_rcv_buf_full, MFP68901_INTERRUPT_RCV_BUF_FULL);
    mfp68901_interrupt_install(uart_rcv_err, MFP68901_INTERRUPT_RCV_ERR);
    mfp68901_interrupt_install(uart_xmit_buf_empty, MFP68901_INTERRUPT_XMIT_BUF_EMPTY);
    mfp68901_interrupt_install(uart_xmit_err, MFP68901_INTERRUPT_XMIT_ERR);

    // Enable UART interrupts
    mfp68901_ier_bitset(
        MFP68901_INTERRUPT_RCV_BUF_FULL |
        MFP68901_INTERRUPT_RCV_ERR |
        MFP68901_INTERRUPT_XMIT_BUF_EMPTY |
        MFP68901_INTERRUPT_XMIT_ERR
    );
    mfp68901_imr_bitset(
        MFP68901_INTERRUPT_RCV_BUF_FULL |
        MFP68901_INTERRUPT_RCV_ERR |
        MFP68901_INTERRUPT_XMIT_BUF_EMPTY |
        MFP68901_INTERRUPT_XMIT_ERR
    );

    // Enable (lower) RTS
    mfp68901_gpdr_set_masked(0, 1U << 7);

    if (dev_uart_major != device_register(dev_uart_major, &dev_uart_dops)) {
        panic("Failed to register uart devices");
    }

    devreg_new("uart0", dev_uart_major, 0);

    return 0;
}

static void dev_uart_deinit(void) {
    device_unregister(dev_uart_major);
}

static int uart_recv(void) {
    // Enable (lower) RTS
    mfp68901_gpdr_set_masked(0, 1U << 7);

    int result = -1;

    thread_scheduler_lock();

    uint8_t c;
    if (circbuf_uint8_t_get(&rx_circbuf, &c)) {
        result = c;
    }

    thread_scheduler_unlock();

    return result;
}

static void uart_send(uint8_t c) {
    // Enable (lower) RTS
    mfp68901_gpdr_set_masked(0, 1U << 7);

    bool has_sent = false;
    while (!has_sent) {
        thread_scheduler_lock();

        enum mfp68901_tsr tsr = mfp68901_tsr_get();

        if (tsr & MFP68901_TSR_BE) {
            mfp68901_udr_set(c);
            has_sent = true;
        } else {
            has_sent = circbuf_uint8_t_put(&tx_circbuf, c);
        }
        
        thread_scheduler_unlock();
    }
}

static ssize_t dev_uart_read(int minor, void *buf, size_t count, unsigned long offset, int *errno_ptr) {
    switch(minor) {
    case 0: // uart0
        for (size_t i = 0; i < count; ++i) {
            int tmp = uart_recv();
            if (tmp >= 0) {
                ((uint8_t *) buf)[i] = tmp;
            } else {
                return i;
            }
        }
        return count;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }
}

static ssize_t dev_uart_write(int minor, const void *buf, size_t count, unsigned long offset, int *errno_ptr) {
    switch(minor) {
    case 0: // uart0
        for (size_t i = 0; i < count; ++i) {
            uart_send(((uint8_t *) buf)[i]);
        }
        return count;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }
}

static int dev_uart_ioctl(int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr) {
    *errno_ptr = EINVAL;
    return -1;
}

__attribute__((interrupt)) static void uart_rcv_buf_full(void) {
    mfp68901_isr_bitclear(MFP68901_INTERRUPT_RCV_BUF_FULL);

    uint8_t c = mfp68901_udr_get();
    circbuf_uint8_t_put(&rx_circbuf, c);
}

__attribute__((interrupt)) static void uart_rcv_err(void) {
    mfp68901_isr_bitclear(MFP68901_INTERRUPT_RCV_ERR);
}

__attribute__((interrupt)) static void uart_xmit_buf_empty(void) {
    mfp68901_isr_bitclear(MFP68901_INTERRUPT_XMIT_BUF_EMPTY);

    uint8_t c;
    if (circbuf_uint8_t_get(&tx_circbuf, &c)) {
        mfp68901_udr_set(c);
    }
}

__attribute__((interrupt)) static void uart_xmit_err(void) {
    mfp68901_isr_bitclear(MFP68901_INTERRUPT_XMIT_ERR);
}
