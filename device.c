#include "device.h"
#include "kalloc.h"
#include "panic.h"
#include "string.h"
#include "vfs.h"

static device *device_new(device_operations *dops);
static void device_delete(device *dev);

device *devices[DEVICE_NUM] = {0};
rwlock devices_rwl = RWLOCK_INIT;

static device *device_new(device_operations *dops) {
    device *dev = kalloc(sizeof(device));

    if (dev) {
        memset(dev, 0, sizeof(*dev));
        dev->dops = dops;
    }

    return dev;
}

static void device_delete(device *dev) {
    memset(dev, 0, sizeof(dev));
    kfree(dev);
}

int device_register(int major, device_operations *dops) {
    device *dev = device_new(dops);
    if (!dev) {
        return -1;
    }

    rwlock_write_begin(&devices_rwl);

    if (major == 0) {
        // Find unused major number
        for (major = 1; major < DEVICE_NUM; ++major) {
            if (!devices[major]) {
                devices[major] = dev;
                break;
            }
        }
        if (major >= DEVICE_NUM) {
            // No free device
            major = -1;
        }
    } else if (major > 0 && major < DEVICE_NUM) {
        // Use specific major number
        if (!devices[major]) {
            devices[major] = dev;
        } else {
            // Already in use
            major = -1;
        }
    } else {
        // Invalid number
        major = -1;
    }

    rwlock_write_end(&devices_rwl);

    if (major < 0) {
        device_delete(dev);
    }
    return major;
}

void device_unregister(int major) {
    rwlock_write_begin(&devices_rwl);

    if (major > 0 && major < DEVICE_NUM) {
        panic("Tried to unregister invalid major device number");
    } else if (!devices[major]) {
        panic("Tried to unregister unused major device number");
    } else {
        device_delete(devices[major]);
        devices[major] = NULL;
    }

    rwlock_write_end(&devices_rwl);
}

ssize_t device_read(int major, int minor, void *buf, size_t count, unsigned long offset, int *errno_ptr) {
    if (major < 0 || major >= DEVICE_NUM) {
        *errno_ptr = EINVAL;
        return -1;
    }

    rwlock_read_begin(&devices_rwl);
    device *this_device = devices[major];
    if (!this_device) {
        rwlock_read_end(&devices_rwl);
        *errno_ptr = EINVAL;
        return -1;
    }
    rwlock_read_end(&devices_rwl);

    // TODO: Make sure this device can't be deleted until this completes
    // TODO: Make sure this actually exists
    return this_device->dops->read(minor, buf, count, offset, errno_ptr);
}

ssize_t device_write(int major, int minor, const void *buf, size_t count, unsigned long offset, int *errno_ptr) {
    if (major < 0 || major >= DEVICE_NUM) {
        *errno_ptr = EINVAL;
        return -1;
    }

    rwlock_read_begin(&devices_rwl);
    device *this_device = devices[major];
    if (!this_device) {
        rwlock_read_end(&devices_rwl);
        *errno_ptr = EINVAL;
        return -1;
    }
    rwlock_read_end(&devices_rwl);

    // TODO: Make sure this device can't be deleted until this completes
    // TODO: Make sure this actually exists
    return this_device->dops->write(minor, buf, count, offset, errno_ptr);
}

int device_ioctl(int major, int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr) {
    if (major < 0 || major >= DEVICE_NUM) {
        *errno_ptr = EINVAL;
        return -1;
    }

    rwlock_read_begin(&devices_rwl);
    device *this_device = devices[major];
    if (!this_device) {
        rwlock_read_end(&devices_rwl);
        *errno_ptr = EINVAL;
        return -1;
    }
    rwlock_read_end(&devices_rwl);

    // TODO: Make sure this device can't be deleted until this completes
    // TODO: Make sure this actually exists
    return this_device->dops->ioctl(minor, request, argp, check_user_pointers, errno_ptr);
}
