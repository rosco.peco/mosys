# Mosys Operating System for rosco-m68k

This is designed for use with the [rosco_m68k](https://rosco-m68k.com/).
Currently builds for version 1.01.
By default, it expects to be in `code/software` in the [rosco_m68k](https://github.com/rosco-m68k/rosco_m68k) repository.
