#ifndef INCLUDE_THREAD_H
#define INCLUDE_THREAD_H

#include "spinlock.h"
#include "kalloc.h"
#include "syscall.h"
#include <stdbool.h>
#include <stdnoreturn.h>

typedef struct thread_list {
    struct thread *head;
    struct thread *tail;
    spinlock lock;
} thread_list;

typedef enum thread_state {
    THREAD_STATE_READY,
    THREAD_STATE_RUNNING,
    THREAD_STATE_TERMINATED,
    THREAD_STATE_SLEEPING,
    THREAD_STATE_MUTEX,
    THREAD_STATE_SEMAPHORE,
    THREAD_STATE_VFORK,
    THREAD_STATE_EXIT,
    THREAD_STATE_WAITPID,
} thread_state;

struct process;

typedef struct thread {
    void *ssp;  // Must be the 1st element
    void *usp;  // Must be the 2nd element; to be used when switching processes when not in a syscall (e.g. interrupts)
    volatile struct syscall_state *user_state;
    struct thread *next;    // Used for sleeping lists
    thread_state state;
    struct ownedmem *sup_stack;
    struct ownedmem *usr_stack;
    struct process *owner_process;
    unsigned long long sleep_until_us;
    thread_list vfork_thread_list;
    thread_list waitpid_thread_list;
    int status;
} thread;

#define THREAD_LIST_INIT { NULL, NULL, SPINLOCK_INIT }

extern thread *thread_current;
extern thread_list thread_ready_list;

void thread_init(void);

thread *thread_new(void);
void thread_delete(thread *thread_ptr);

void thread_setup(thread *thread_ptr, void (*func)(), struct ownedmem *sup_stack, struct ownedmem *usr_stack);
void thread_setup_clone(thread *thread_ptr);
void thread_setup_stack(thread *thread_ptr, void (*func)());
noreturn void thread_idle_loop (void);

void thread_scheduler_lock(void);
void thread_scheduler_unlock(void);
void thread_differ_switches(void);
void thread_undiffer_switches(void);

void thread_block(thread_state state);
bool thread_block_while_unblocking_from_list(thread_state state, thread_list *unblock_from_list);
size_t thread_block_while_unblocking_all_from_list(thread_state state, thread_list *unblock_from_list);
void thread_block_to_list(thread_state state, thread_list *block_to_list);
bool thread_block_to_list_while_unblocking_from_list(thread_state state, thread_list *block_to_list, thread_list *unblock_from_list);
size_t thread_block_to_list_while_unblocking_all_from_list(thread_state state, thread_list *block_to_list, thread_list *unblock_from_list);
void thread_unblock(thread *unblocked_thread);
bool thread_unblock_from_list(thread_list *list);
size_t thread_unblock_all_from_list(thread_list *list);

thread *thread_switch(thread *thread_ptr);
void thread_schedule(void);
void thread_terminate(void);

void thread_list_enqueue(thread_list *list, thread *enq_thread);
thread *thread_list_dequeue(thread_list *list);

#endif
