#ifndef INCLUDE_KALLOC_H
#define INCLUDE_KALLOC_H

#include <stddef.h>

typedef struct { void *start; void *end; } ptr_range;
#define PTR_RANGE_NULL ((ptr_range) { NULL, NULL })

ptr_range kalloc_range(size_t size);
void* kalloc(size_t size);
void* kalloc_end(size_t size);
ptr_range kalloc_clone(ptr_range orig);
ptr_range kalloc_clone_starting_at(ptr_range orig, void *starting_at);
ptr_range kalloc_clone_ending_at(ptr_range orig, void *ending_at);
void kfree(void *ptr);

#endif
