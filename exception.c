#include "exception.h"
#include "process.h"
#include "processor.h"
#include "thread.h"
#include <stdio.h>

static void exception_bus_error(long arg);
static void exception_address_error(long arg);
static void exception_illegal_instruction(long arg);
static void exception_zero_divide(long arg);
static void exception_chk_instruction(long arg);
static void exception_trapv_instruction(long arg);
static void exception_privilege_violation(long arg);
static void exception_format_error(long arg);
static void exception_spurious_interrupt(long arg);

static void exception_trace(long arg);

void exception_init(void) {
    exception_vector_replace(exception_bus_error, 2);
    exception_vector_replace(exception_address_error, 3);
    exception_vector_replace(exception_illegal_instruction, 4);
    exception_vector_replace(exception_zero_divide, 5);
    exception_vector_replace(exception_chk_instruction, 6);
    exception_vector_replace(exception_trapv_instruction, 7);
    exception_vector_replace(exception_privilege_violation, 8);
    exception_vector_replace(exception_format_error, 14);
    exception_vector_replace(exception_spurious_interrupt, 24);

    exception_vector_replace(exception_trace, 9);
}

void exception_stack_frame_print(volatile struct exception_stack_frame *esf) {
    int pid = -1;
    if (thread_current && thread_current->owner_process) {
        pid = thread_current->owner_process->pid;
    }

    printf("SR: 0x%04hX, PC: 0x%08lX, F: 0x%01hX, VO: 0x%03hX, SSP: 0x%p, USP: 0x%p, PID: %d\n",
           (unsigned short) esf->sr,
           (unsigned long) esf->pc,
           (unsigned short) (esf->format_vector_offset >> 12),
           (unsigned short) (esf->format_vector_offset & 0xFFFU),
           (void *) (esf + 1),
           usp_get(),
           pid);
}

__attribute__((interrupt)) static void exception_bus_error(long arg) {
    volatile struct exception_stack_frame *esf = (void *) (&arg - 1);
    thread_scheduler_lock();
    printf("Bus Error: ");
    exception_stack_frame_print(esf);
    thread_scheduler_unlock();
    while (1)
        ;
}

__attribute__((interrupt)) static void exception_address_error(long arg) {
    volatile struct exception_stack_frame *esf = (void *) (&arg - 1);
    thread_scheduler_lock();
    printf("Address Error: ");
    exception_stack_frame_print(esf);
    thread_scheduler_unlock();
    while (1)
        ;
}

__attribute__((interrupt)) static void exception_illegal_instruction(long arg) {
    volatile struct exception_stack_frame *esf = (void *) (&arg - 1);
    thread_scheduler_lock();
    printf("Illegal Instruction: ");
    exception_stack_frame_print(esf);
    thread_scheduler_unlock();
    while (1)
        ;
}

__attribute__((interrupt)) static void exception_zero_divide(long arg) {
    volatile struct exception_stack_frame *esf = (void *) (&arg - 1);
    thread_scheduler_lock();
    printf("Zero Divide: ");
    exception_stack_frame_print(esf);
    thread_scheduler_unlock();
    while (1)
        ;
}

__attribute__((interrupt)) static void exception_chk_instruction(long arg) {
    volatile struct exception_stack_frame *esf = (void *) (&arg - 1);
    thread_scheduler_lock();
    printf("CHK Instruction: ");
    exception_stack_frame_print(esf);
    thread_scheduler_unlock();
    while (1)
        ;
}

__attribute__((interrupt)) static void exception_trapv_instruction(long arg) {
    volatile struct exception_stack_frame *esf = (void *) (&arg - 1);
    thread_scheduler_lock();
    printf("TRAPV Instruction: ");
    exception_stack_frame_print(esf);
    thread_scheduler_unlock();
    while (1)
        ;
}

__attribute__((interrupt)) static void exception_privilege_violation(long arg) {
    volatile struct exception_stack_frame *esf = (void *) (&arg - 1);
    thread_scheduler_lock();
    printf("Privilege Violation: ");
    exception_stack_frame_print(esf);
    thread_scheduler_unlock();
    while (1)
        ;
}

__attribute__((interrupt)) static void exception_format_error(long arg) {
    volatile struct exception_stack_frame *esf = (void *) (&arg - 1);
    thread_scheduler_lock();
    printf("Format Error: ");
    exception_stack_frame_print(esf);
    thread_scheduler_unlock();
    while (1)
        ;
}

__attribute__((interrupt)) static void exception_spurious_interrupt(long arg) {
    volatile struct exception_stack_frame *esf = (void *) (&arg - 1);
    thread_scheduler_lock();
    printf("Spurious Interrupt: ");
    exception_stack_frame_print(esf);
    thread_scheduler_unlock();
    while (1)
        ;
}

__attribute__((interrupt)) static void exception_trace(long arg) {
    volatile struct exception_stack_frame *esf = (void *) (&arg - 1);
    thread_scheduler_lock();
    printf("Trace: ");
    exception_stack_frame_print(esf);
    thread_scheduler_unlock();
}
