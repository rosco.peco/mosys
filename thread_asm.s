    export  thread_setup_stack
    export  thread_switch
    import  thread_on_start
    import  thread_pre_switch
    import  thread_post_switch
    
    section .text

; Return Address
; A6
; A5
; A4
; A3
; A2
; D7
; D6
; D5
; D4
; D3
; D2

; void thread_setup_stack(thread *thread_ptr, void (*func)());
thread_setup_stack:
    move.l  (4,SP),A0               ; thread_ptr to A0
    move.l  (0,A0),A1               ; thread_ptr->ssp to A1

    move.l  (8,SP),-(A1)            ; Push func on A1
    move.l  #thread_on_start,-(A1)  ; Push thread_on_start on A1
    movem.l D2-D7/A2-A6,-(A1)       ; Push saved registers on A1

    move.l  A1,(0,A0)               ; A1 to thread_ptr->ssp

    rts

; thread_control_block *thread_switch(thread *thread_ptr);
; Only to be called by thread_schedule()
thread_switch:
    move.l  (4,SP),A0               ; thread_ptr to A0

    movem.l D2-D7/A2-A6,-(SP)       ; Push saved registers

    move.l  A0,A2                   ; thread_ptr to A2

    jsr     thread_pre_switch

    move.l  thread_current,A3       ; thread_current to A3
    move.l  SP,(0,A3)               ; SSP to thread_current->ssp
    move.l  USP,A4                  ; USP to A4
    move.l  A4,(4,A3)               ; USP in A4 to thread_current->usp

    move.l  A2,thread_current       ; thread_ptr to thread_current
    move.l  (4,A2),A4               ; thread_ptr->usp to A4
    move.l  A4,USP                  ; thread_ptr->usp in A4 to USP
    move.l  (0,A2),SP               ; thread_ptr->ssp to SSP

    jsr     thread_post_switch

    move.l  A3,D0                   ; Old thread_current to D0

    movem.l (SP)+,D2-D7/A2-A6       ; Pop saved registers

    rts
