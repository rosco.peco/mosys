#include "devreg.h"
#include "panic.h"
#include <string.h>

static devreg *devreg_list = NULL;
static rwlock devreg_list_rwl = RWLOCK_INIT;

devreg *devreg_new(const char *name, int major, int minor) {
    devreg *dr = kalloc(sizeof(devreg));

    if (dr) {
        memset(dr, 0, sizeof(*dr));
        rwlock_init(&dr->rwl);
        dr->ref_count = 1;
        dr->name = name;
        dr->major = major;
        dr->minor = minor;
    }

    rwlock_write_begin(&devreg_list_rwl);

    // Advance next_ptr to point to the current NULL pointer in list
    devreg **next_ptr = &devreg_list;
    while (*next_ptr != NULL) {
        next_ptr = &((*next_ptr)->next);
    }

    // Add fst to list
    dr->next = NULL;
    *next_ptr = dr;

    rwlock_write_end(&devreg_list_rwl);

    return dr;
}

void devreg_delete(devreg *dr) {
    // TODO: Unchain from linked list
    panic("Deleting devreg not supported");
    if (dr->ref_count != 0) {
        panic("Tried to delete still-referenced devreg");
    }
    kfree(dr);
}

void devreg_ref(devreg *dr) {
    rwlock_write_begin(&dr->rwl);

    if (dr->ref_count == SIZE_MAX) {
        panic("Overflowed devreg ref_count");
    }
    dr->ref_count += 1;

    rwlock_write_end(&dr->rwl);
}

void devreg_unref(devreg *dr) {
    rwlock_write_begin(&dr->rwl);

    if (dr->ref_count == 0) {
        panic("Underflowed devreg ref_count");
    }
    dr->ref_count -= 1;
    
    if (dr->ref_count == 0) {
        devreg_delete(dr);
        dr = NULL;
    } else {
        rwlock_write_end(&dr->rwl);
    }
}


devreg *devreg_lookup(const char *name) {
    rwlock_read_begin(&devreg_list_rwl);

    devreg *dr;
    for (dr = devreg_list; dr; dr = dr->next) {
        if (strcmp(dr->name, name) == 0) {
            break;
        }
    }

    if (dr)
        devreg_ref(dr);

    rwlock_read_end(&devreg_list_rwl);

    return dr;
}

devreg *devreg_get_nth(unsigned long n) {
    rwlock_read_begin(&devreg_list_rwl);

    devreg *dr = devreg_list;
    for (; n > 0 && dr; --n) {
        dr = dr->next;
    }

    if (dr)
        devreg_ref(dr);

    rwlock_read_end(&devreg_list_rwl);

    return dr;
}
