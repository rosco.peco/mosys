#ifndef INCLUDE_OWNEDMEM_H
#define INCLUDE_OWNEDMEM_H

#include "kalloc.h"
#include "mutex.h"
#include <stdint.h>

typedef struct ownedmem {
    mutex mtx;
    size_t ref_count;

    ptr_range range;
} ownedmem;

ownedmem *ownedmem_new_from_ptr_range(ptr_range range);
ownedmem *ownedmem_new(size_t size);
void ownedmem_delete(ownedmem *owndmm);
ownedmem *ownedmem_ref(ownedmem *owndmm);
void ownedmem_unref(ownedmem *owndmm);
ownedmem *ownedmem_clone(ownedmem *owndmm_src);
ownedmem *ownedmem_clone_starting_at(ownedmem *owndmm_orig, void *starting_at);
ownedmem *ownedmem_clone_ending_at(ownedmem *owndmm_orig, void *ending_at);

#endif
