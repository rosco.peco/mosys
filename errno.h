#ifndef INCLUDE_ERRNO_H
#define INCLUDE_ERRNO_H

enum {
    EOK = 0,
    EPERM,
    ENOENT,
    EINVAL,
    ENOSYS,
    EBADF,
    EFAULT,
    ENOSPC,
    ENOMEM,
    ENOEXEC,
    ECHILD,
    ENOTDIR,
    ENOTTY,
};

#endif
