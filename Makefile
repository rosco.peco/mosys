MOSYSDIR ?= .
ROSCODIR ?= $(MOSYSDIR)/../../..
SYSDIR ?= $(ROSCODIR)/code/software/libs/build
SYSINCDIR ?= ${SYSDIR}/include
SYSLIBDIR ?= ${SYSDIR}/lib
USERDIR ?= $(MOSYSDIR)/user

TARGET ?= mosys.bin
OBJS := \
	kmain.o kalloc.o thread.o thread_asm.o mutex.o	\
	panic.o elf.o process.o process_asm.o syscall.o \
	syscall_asm.o timing.o module.o vfs.o rwlock.o 	\
	device.o devreg.o fs_devfs.o dev_blackhole.o 	\
	dev_uart.o circbuf.o fs_initramfs.o tar.o		\
	exception.o ownedmem.o dev_spidev.o dev_at25.o
INITRD_DIRS ?= dev/
INITRD_FILES ?= $(USERDIR)/init/init.elf $(USERDIR)/sh/sh.elf $(USERDIR)/args/args.elf $(USERDIR)/ls/ls.elf
LDSCRIPT ?= mosys.ld
INITRD_H ?= initrd.h
INITRD_TAR ?= initrd.tar
INITRD_DIR ?= initrd
USER_LIBS ?= libmosys libsysprintf
USER_PROGS ?= init sh args ls
USER_ALL = $(USER_DIRS) $(USER_LIBS) $(USER_PROGS)
USER_LIB_DIRS := $(patsubst %, $(USERDIR)/%, $(USER_LIBS))
USER_PROG_DIRS := $(patsubst %, $(USERDIR)/%, $(USER_PROGS))

LIBS = -lprintf -lcstdlib -lrtlsupport -lmachine -lstart_serial
DEFINES = -DROSCO_M68K

CFLAGS = \
	-std=gnu11 -ffreestanding -Wall -pedantic -Werror			\
	-I $(SYSINCDIR)	-I $(MOSYSDIR)								\
	-mcpu=68010 -march=68010 -mtune=68010						\
	-mno-align-int -mno-strict-align $(DEFINES) -O2 -MD -MP

LDFLAGS = \
	-T $(LDSCRIPT) -L $(SYSLIBDIR)	 \
	-Map=$(MAP) --pic-executable -N

ASFLAGS = -Felf -m68010 -quiet $(DEFINES) -dependall=make -depfile $(@:.o=.d)

CC = m68k-elf-gcc
LD = m68k-elf-ld
AR = m68k-elf-ar
RANLIB = m68k-elf-ranlib
AS = vasmm68k_mot
RM_F = rm -f
RM_RF = $(RM_F) -r
MKDIR_P = mkdir -p
CP_R = cp -r

DEPS := $(OBJS:.o=.d)
MAP = $(TARGET:.bin=.map)

$(TARGET): $(OBJS) | $(LDSCRIPT)
	$(LD) $(LDFLAGS) $^ -o $@ $(LIBS)
	chmod a-x $@

.PHONY: all clean $(USER_PROG_DIRS) $(USER_LIB_DIRS)

all: $(TARGET)

clean: 
	$(RM_F) $(OBJS) $(DEPS) $(TARGET) $(MAP) $(INITRD_H) $(INITRD_TAR) 
	$(RM_RF) $(INITRD_DIR)
	for user_dir in $(USER_ALL); do \
		$(MAKE) -C $(USERDIR)/$$user_dir clean; \
	done

$(INITRD_TAR): $(INITRD_FILES)
	$(MKDIR_P) $(INITRD_DIR)
	$(MKDIR_P) $(patsubst %, $(INITRD_DIR)/%, $(INITRD_DIRS))
	$(CP_R) $^ $(INITRD_DIR)/
	tar -cf $@ -C $(INITRD_DIR) $(INITRD_DIRS) $(notdir $^)

$(INITRD_H): $(INITRD_TAR)
	xxd -i $< $@

$(USER_LIB_DIRS):
	$(MAKE) -C $@

$(USERDIR)/init/init.elf: $(USERDIR)/init
	$(MAKE) -C $< init.elf

$(USERDIR)/sh/sh.elf: $(USERDIR)/sh
	$(MAKE) -C $< sh.elf

$(USERDIR)/args/args.elf: $(USERDIR)/args
	$(MAKE) -C $< args.elf

$(USERDIR)/ls/ls.elf: $(USERDIR)/ls
	$(MAKE) -C $< ls.elf

fs_initramfs.c: initrd.h

$(USER_PROG_DIRS): | $(USER_LIB_DIRS)

-include $(DEPS)
