#include "panic.h"
#include <stdio.h>

noreturn void panic_func(const char *file, const char *line, const char *reason) {
    asm ("ori #(7 << 8), %SR");

    printf("KERNEL PANIC at %s:%s\nReason: %s\n", file, line, reason);

    while (1) {
        // Spin forever
    }
}
