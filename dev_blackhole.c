#include "device.h"
#include "devreg.h"
#include "errno.h"
#include "module.h"
#include "panic.h"
#include "string.h"

// TODO: Remove this
void* memmove(void* dstptr, const void* srcptr, size_t size) {
	unsigned char* dst = (unsigned char*) dstptr;
	const unsigned char* src = (const unsigned char*) srcptr;
	if (dst < src) {
		for (size_t i = 0; i < size; i++)
			dst[i] = src[i];
	} else {
		for (size_t i = size; i != 0; i--)
			dst[i-1] = src[i-1];
	}
	return dstptr;
}

static int dev_blackhole_init(void);
static void dev_blackhole_deinit(void);

static ssize_t dev_blackhole_read(int minor, void *buf, size_t count, unsigned long offset, int *errno_ptr);
static ssize_t dev_blackhole_write(int minor, const void *buf, size_t count, unsigned long offset, int *errno_ptr);
static int dev_blackhole_ioctl(int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr);

static module dev_blackhole = {
    .init = dev_blackhole_init,
    .deinit = dev_blackhole_deinit,
    .name = "dev_blackhole",
};
MODULE_DEFINE(dev_blackhole);

static const int dev_blackhole_major = 1;

static device_operations dev_blackhole_dops = {
    dev_blackhole_read,
    dev_blackhole_write,
    dev_blackhole_ioctl,
};

static int dev_blackhole_init(void) {
    if (dev_blackhole_major != device_register(dev_blackhole_major, &dev_blackhole_dops)) {
        panic("Failed to register blackhole devices");
    }

    devreg_new("null", dev_blackhole_major, 0);
    devreg_new("zero", dev_blackhole_major, 1);
    devreg_new("full", dev_blackhole_major, 2);
    devreg_new("mem",  dev_blackhole_major, 3);

    return 0;
}

static void dev_blackhole_deinit(void) {
    device_unregister(dev_blackhole_major);
}

static ssize_t dev_blackhole_read(int minor, void *buf, size_t count, unsigned long offset, int *errno_ptr) {
    switch(minor) {
    case 0: // null
        return 0;
    case 1: // zero
    case 2: // full
        memset(buf, 0, count);
        return count;
    case 3: // mem
        memmove(buf, (const void *) offset, count);
        return count;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }
}

static ssize_t dev_blackhole_write(int minor, const void *buf, size_t count, unsigned long offset, int *errno_ptr) {
    switch(minor) {
    case 0: // null
    case 1: // zero
        return count;
    case 2: // full
        *errno_ptr = ENOSPC;
        return -1;
    case 3: // mem
        memmove((void *) offset, buf, count);
        return count;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }
}

static int dev_blackhole_ioctl(int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr) {
    *errno_ptr = EINVAL;
    return -1;
}
