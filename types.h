#ifndef INCLUDE_TYPES_H
#define INCLUDE_TYPES_H

typedef unsigned long time_t;
struct timespec {
    time_t tv_sec;
    long tv_nsec;
};

typedef unsigned short mode_t;
typedef signed long ssize_t;
typedef unsigned long ino_t;

#endif
