#include "device.h"
#include "devreg.h"
#include "kalloc.h"
#include "initrd.h"
#include "module.h"
#include "panic.h"
#include "tar.h"
#include "vfs.h"
#include <stdatomic.h>
#include <stdbool.h>
#include <string.h>

static int initramfs_init(void);
static void initramfs_deinit(void);

static superblock *initramfs_fsops_mount(const char *source, unsigned long mountflags, const void *data);
static vnode *initramfs_fsops_lookup(superblock *sb, const char *subpath);
static void initramfs_fsops_destroy(superblock *sb, vnode *vn);

static ssize_t initramfs_fops_read(fdesc *fdsc, void *buf, size_t count, int *errno_ptr);
static ssize_t initramfs_fops_getdents(fdesc *fdsc, dirent *drnt, size_t count, int *errno_ptr);

static atomic_bool initramfs_is_init = false;

static module initramfs = {
    .init = initramfs_init,
    .deinit = initramfs_deinit,
    .name = "initramfs",
};
MODULE_DEFINE(initramfs);

static fs_operations initramfs_fsops = {
    .mount = initramfs_fsops_mount,
    .lookup = initramfs_fsops_lookup,
    .destroy = initramfs_fsops_destroy,
};

static file_operations initramfs_fops = {
    .read = initramfs_fops_read,
    .getdents = initramfs_fops_getdents,
};

static filesystem_type initramfs_fstype = {
    .name = "initramfs",
    .fsops = &initramfs_fsops,
};

static int initramfs_init(void) {
    vfs_filesystem_type_register(&initramfs_fstype);

    // Create dummy superblock
    superblock *sb = initramfs_fsops_mount(NULL, 0, initrd_tar);
    if (!sb)
        panic("Failed to mount initramfs");

    // Create root vnode
    vnode *vn_root = vfs_vnode_new(sb);
    if (!vn_root)
        panic("Failed to create initramfs root vnode");

    vn_root->type = FILE_TYPE_DIRECTORY;

    vn_root->inode = 0;

    vn_root->fops = &initramfs_fops;

    // Link root vnode in superblock 
    sb->mounted = vn_root;

    // Create mountpoint
    mountpoint *mp = vfs_mountpoint_new(sb, "/");
    if (!mp)
        panic("Failed to create initramfs mountpoint");

    initramfs_is_init = true;
    return 0;
}

static void initramfs_deinit(void) {
    initramfs_is_init = false;

    // TODO: Figure out what to actually do here
    (void) initramfs_fops;
}

// Data should be a pointer to a static tar fdsc
static superblock *initramfs_fsops_mount(const char *source, unsigned long mountflags, const void *data) {
    superblock *sb = vfs_superblock_new(&initramfs_fstype);
    if (!sb)
        return NULL;
    sb->data = (void *) data;

    return sb;
}

static vnode *initramfs_fsops_lookup(superblock *sb, const char *subpath) {
    if (0 == strcmp("", subpath)) {
        vfs_vnode_ref(sb->mounted);
        return sb->mounted;
    } else {
        size_t found_inode;
        struct tar_entry *found_entry = tar_lookup_name(sb->data, subpath, &found_inode);
        if (!found_entry) {
            return NULL;
        }

        vnode *vn = vfs_vnode_new(sb);
        if (!vn) {
            return NULL;
        }

        mutex_lock(&vn->mtx);
        vn->type = FILE_TYPE_PLAIN;

        vn->inode = found_inode;
        
        vn->fops = &initramfs_fops;
        mutex_unlock(&vn->mtx);
        return vn;
    }
}

static void initramfs_fsops_destroy(superblock *sb, vnode *vn) {
    vfs_vnode_unref(vn);
}

static ssize_t initramfs_fops_read(fdesc *fdsc, void *buf, size_t count, int *errno_ptr) {
    struct tar_entry *entry = tar_get_nth_entry(fdsc->vn->sb->data, fdsc->vn->inode);

    size_t size = tar_oct_atoi(entry->header.size, sizeof(entry->header.size) - 1);
    size_t offset = fdsc->offset;
    if (size <= offset)
        return 0;
    size_t size_left = size - offset;
    if (count > size_left) {
        count = size_left;
    }

    memcpy(buf, (const char *) entry->contents + offset, count);

    return count;
}

static ssize_t initramfs_fops_getdents(fdesc *fdsc, dirent *drnt, size_t count, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_DIRECTORY)
        panic("Expected directory");
    if (fdsc->vn != fdsc->vn->sb->mounted)
        panic("Expected initramfs root as only directory");

    size_t done_count = 0;
    while (1) {
        struct tar_entry *entry = tar_get_nth_entry(fdsc->vn->sb->data, fdsc->offset);
        if (!entry)
            break;
        size_t needed_count = sizeof(dirent);
        if (needed_count <= count) {
            count -= needed_count;
            drnt->ino = fdsc->offset;
            strncpy(drnt->name, entry->header.name, sizeof(drnt->name));
            drnt->name[sizeof(drnt->name) - 1] = '\0';
            drnt = (dirent *) ((char *) drnt + needed_count);
            done_count += needed_count;
        } else {
            if (done_count == 0) {
                *errno_ptr = EINVAL;
                done_count = -1;
            }
            break;
        }
        fdsc->offset += 1;
    }

    return done_count;
}
